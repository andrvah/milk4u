export interface Expiration {
  id?: number;
  temperature:	number;
  daysAmount: number;
}
