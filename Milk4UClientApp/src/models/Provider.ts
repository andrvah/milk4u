import { Contact } from './Contact';

export interface Provider {
  id?: number;
  rating?: number;
  contact?: Contact;
}
