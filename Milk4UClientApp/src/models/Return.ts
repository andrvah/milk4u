export interface Return {
  id?: number;
  reason?: string;
  date?: string;
  orderId?: number;
}
