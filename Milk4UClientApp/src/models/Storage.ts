import { Essential } from './Essentials';

export interface Storage {
  id?: number;
  weight?: number;
  date?: string;
  essential?: Essential;
  essetialName?: string;
}
