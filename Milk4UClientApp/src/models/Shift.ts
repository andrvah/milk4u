import { Employee } from './Employee';
import { Schedule } from './Schedule';

export interface Shift {
  id?: number;
  workersNumber?: number;
  date?: string;
  employee?: Employee[];
  shiftsSchedule?: Schedule[];
}
