export interface Nutritional {
  id?:	number;
  fats:	number;
  proteins:	number;
  carbohydrates:	number;
  calories:	number;
}
