import { Contact } from './Contact';

export interface Saler {
  id?: number;
  contact?: Contact;
}
