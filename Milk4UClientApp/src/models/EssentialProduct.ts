import { Essential } from './Essentials';

export interface EssentialProduct {
  id?: number;
  essential?: Essential;
  weight?: number;
}
