import { Essential } from './Essentials';

export interface EssentialsEssentialOrders {
  id?: number;
  weight?: number;
  essential?: Essential;
}
