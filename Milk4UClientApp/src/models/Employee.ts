import { Contact } from './Contact';

export interface Employee {
  id?: number;
  position?:	string;
  salary?: number;
  contact?: Contact;
  shiftId?: number;
  firstName?: string;
  lastName?: string;
}
