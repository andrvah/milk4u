export interface OrdersProducts {
  productId?: number;
  amount?: number;
  productName?: string;
}
