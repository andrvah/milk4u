import { Product } from './Product';

export interface Warehouse {
  id?: number;
  amount?: number;
  date?: string;
  product?: Product;
  productName?: string;
  shiftId?: string;
}
