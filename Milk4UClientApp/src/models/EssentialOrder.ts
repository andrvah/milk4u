import { Provider } from './Provider';
import { EssentialsEssentialOrders } from './EssentialsEssentialOrders';

export interface EssentialOrder {
  id?: number;
  price?: number;
  date?: string;
  provider?: Provider;
  essentialsEssentialOrders?: Array<EssentialsEssentialOrders>;
}
