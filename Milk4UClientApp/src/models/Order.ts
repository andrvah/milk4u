import { Customer } from './Customer';
import { Saler } from './Saler';
import { Return } from './Return';
import { OrdersProducts } from './OrdersProducts';

export interface Order {
  id?: number;
  price?: number;
  date?: string;
  customer?: Customer;
  saler?: Saler;
  returns?: Return[];
  returned?: boolean;
  customerId?: number;
  customerName?: string;
  salerId?: number;
  salerName?: string;
  ordersProducts?: OrdersProducts[];
}
