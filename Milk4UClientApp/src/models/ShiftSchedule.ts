export interface ShiftSchedule {
  id?: number;
  shiftId?: number;
  date?: string;
}
