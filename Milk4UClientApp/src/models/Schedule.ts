export interface Schedule {
  id?: number;
  shiftId?: number;
  date?: string;
}
