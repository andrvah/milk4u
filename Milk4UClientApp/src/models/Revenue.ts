export interface Revenue {
  id?: number;
  revenue?: number;
  incomes?: number;
  expenses?: number;
  startDate: string;
  endDate: string;
}
