import { Contact } from './Contact';

export interface Customer {
  id?: number;
  discount?: number;
  contact?: Contact;
}
