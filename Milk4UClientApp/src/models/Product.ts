import { Expiration } from './Expiration';
import { Nutritional } from './Nutritional';
import { Package } from './Package';
import { Shift } from './Shift';
import { EssentialProduct } from './EssentialProduct';

export interface Product {
  id?:	number;
  name:	string;
  price:	number;
  expiration?: Expiration;
  expirationDays?: string;
  nutritional?: Nutritional;
  caloriesAmount?: string;
  package?: Package;
  packageVolume?: string;
  shift?: Shift;
  productsEssentals?: EssentialProduct[];
}
