import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatDividerModule,
  MatFormFieldModule,
  MatIconModule, MatInputModule,
  MatMenuModule, MatPaginatorModule, MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatButtonModule,
  MatDialogModule,
  MatAutocompleteModule,
  MatExpansionModule,
  MatSelectModule,
  MatDatepickerModule,
  MatNativeDateModule
} from '@angular/material';
import { LoginComponent } from './login/login.component';
import { EssentialsComponent } from './essentials/essentials.component';
import { OrdersComponent } from './orders/orders.component';
import { ProductsComponent } from './products/products.component';
import { RevenueComponent } from './revenue/revenue.component';
import { ShiftsComponent } from './shifts/shifts.component';
import { StorageComponent } from './storage/storage.component';
import { WarehouseComponent } from './warehouse/warehouse.component';
import { RoutingModule } from './routing.module';
import { HttpClientModule } from '@angular/common/http';
import { DeleteProductComponent } from './products/delete-product/delete-product.component';
import { AddProductComponent } from './products/add-product/add-product.component';
import { ReactiveFormsModule } from '@angular/forms';
import { EditProductComponent } from './products/edit-product/edit-product.component';
import { CustomAutocompleteComponent } from './custom-autocomplete/custom-autocomplete.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { AddRevenueComponent } from './revenue/add-revenue/add-revenue.component';
import { DeleteRevenueComponent } from './revenue/delete-revenue/delete-revenue.component';
import { AddEssentialsComponent } from './essentials/add-essentials/add-essentials.component';
import { DeleteEssentialsComponent } from './essentials/delete-essentials/delete-essentials.component';
import { AddOrderComponent } from './orders/add-order/add-order.component';
import { ReturnOrderComponent } from './orders/return-order/return-order.component';
import { ProduceProductComponent } from './warehouse/produce-product/produce-product.component';
import { CleanWarehouseComponent } from './warehouse/clean-warehouse/clean-warehouse.component';
import { CleanStorageComponent } from './storage/clean-storage/clean-storage.component';
import { OrderEssentialsComponent } from './storage/order-essentials/order-essentials.component';
import { AddShiftComponent } from './shifts/add-shift/add-shift.component';
import { DeleteShiftComponent } from './shifts/delete-shift/delete-shift.component';
import { EditShiftComponent } from './shifts/edit-shift/edit-shift.component';
import { FireEmployeeComponent } from './employee/fire-employee/fire-employee.component';
import { EmployeeComponent } from './employee/employee.component';
import { AddEmployeeComponent } from './employee/add-employee/add-employee.component';
import { EditEmployeeComponent } from './employee/edit-employee/edit-employee.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    EssentialsComponent,
    OrdersComponent,
    ProductsComponent,
    RevenueComponent,
    ShiftsComponent,
    StorageComponent,
    WarehouseComponent,
    DeleteProductComponent,
    AddProductComponent,
    EditProductComponent,
    CustomAutocompleteComponent,
    CreateUserComponent,
    AddRevenueComponent,
    DeleteRevenueComponent,
    AddEssentialsComponent,
    DeleteEssentialsComponent,
    AddOrderComponent,
    ReturnOrderComponent,
    ProduceProductComponent,
    CleanWarehouseComponent,
    CleanStorageComponent,
    OrderEssentialsComponent,
    AddShiftComponent,
    DeleteShiftComponent,
    EditShiftComponent,
    FireEmployeeComponent,
    EmployeeComponent,
    AddEmployeeComponent,
    EditEmployeeComponent
  ],
  imports: [
    RoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatToolbarModule,
    MatButtonModule,
    MatTabsModule,
    MatMenuModule,
    HttpClientModule,
    MatDividerModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatSortModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatExpansionModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  entryComponents: [
    DeleteProductComponent,
    AddProductComponent,
    EditProductComponent,
    AddRevenueComponent,
    DeleteRevenueComponent,
    AddEssentialsComponent,
    DeleteEssentialsComponent,
    AddOrderComponent,
    ReturnOrderComponent,
    ProduceProductComponent,
    CleanWarehouseComponent,
    CleanStorageComponent,
    OrderEssentialsComponent,
    AddShiftComponent,
    DeleteShiftComponent,
    EditShiftComponent,
    FireEmployeeComponent,
    AddEmployeeComponent,
    EditEmployeeComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
