import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { EssentialsComponent } from './essentials/essentials.component';
import { OrdersComponent } from './orders/orders.component';
import { ProductsComponent } from './products/products.component';
import { RevenueComponent } from './revenue/revenue.component';
import { ShiftsComponent } from './shifts/shifts.component';
import { StorageComponent } from './storage/storage.component';
import { WarehouseComponent } from './warehouse/warehouse.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { AuthGuardService } from './services/auth-guard.service';
import { EmployeeComponent } from './employee/employee.component';

export const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'addUser',
    component: CreateUserComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'essentials',
    component: EssentialsComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'orders',
    component: OrdersComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'products',
    component: ProductsComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'revenue',
    component: RevenueComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'shifts',
    component: ShiftsComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'storage',
    component: StorageComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'warehouse',
    component: WarehouseComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'employee',
    component: EmployeeComponent,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class RoutingModule { }
