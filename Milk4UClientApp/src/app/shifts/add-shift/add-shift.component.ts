import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { ShiftsService } from 'src/app/services/shifts.service';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Shift } from 'src/models/Shift';
import { Employee } from 'src/models/Employee';
import { ShiftSchedule } from 'src/models/ShiftSchedule';
import { Schedule } from 'src/models/Schedule';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-add-shift',
  templateUrl: './add-shift.component.html',
  styleUrls: ['./add-shift.component.scss']
})
export class AddShiftComponent implements OnInit {
  addShiftForm: FormGroup;
  errorMessage = '';
  shift: Shift;
  employee: Employee[];
  employeeStrings = [];
  shiftsSchedule: ShiftSchedule[];

  constructor(
    private formBuilder: FormBuilder,
    private service: ShiftsService,
    private employeeService: EmployeeService,
    private matDialogRef: MatDialogRef<AddShiftComponent>
  ) {
  }

  ngOnInit() {
    this.addShiftForm = new FormGroup({
      employee: new FormArray([]),
      shiftsSchedule: new FormArray([])
    });

    this.employeeService.get().subscribe( result => {
      result.forEach(element => {
        this.employeeStrings.push(`${element.id}: ${element.contact.firstName} ${element.contact.lastName} - ${element.position}`);
      });
    });
  }

  createVacancy() {
    let empId;
    let formGroup;
    let index;

    this.shift = {
      employee: [],
      shiftsSchedule: new Array<Schedule>()
    };

    const employeeArray = (this.addShiftForm.get('employee') as FormArray);
    for (let i = 0; i < employeeArray.length; i++) {
      formGroup = employeeArray.at(i);
      index = formGroup.get('employeeId').value.indexOf(':');
      empId = formGroup.get('employeeId').value.slice(0, index);

      const emp: Employee = {
        id: +empId
      };
      this.shift.employee.push(emp);
    }

    const scheduleArray = (this.addShiftForm.get('shiftsSchedule') as FormArray);
    for (let i = 0; i < scheduleArray.length; i++) {
      const value = scheduleArray.at(i).value;
      this.shift.shiftsSchedule.push({date: value.toJSON()});
    }

    console.log(this.addShiftForm);
    if (this.addShiftForm.valid) {
      this.service.add(this.shift).subscribe(response => {
        this.matDialogRef.close();
      }, error => {
        console.log(error);
        this.errorMessage = error.message;
        this.scrollToError();
      });
    } else {
      console.log(this.addShiftForm);
      this.errorMessage = 'Будь ласка, заповніть всі необхідні поля!';
      this.scrollToError();
    }
  }

  addEmployee(): void {
    const control = new FormGroup({
      employeeId: new FormControl(null),
    });
    (this.addShiftForm.get('employee') as FormArray).push(control);
  }

  addDate(): void {
    const control = new FormControl(null, Validators.required);
    (this.addShiftForm.get('shiftsSchedule') as FormArray).push(control);
  }

  deleteEmployee(index: number) {
    (this.addShiftForm.get('employee') as FormArray).removeAt(index);
  }

  deleteDate(index: number) {
    (this.addShiftForm.get('shiftsSchedule') as FormArray).removeAt(index);
  }

  scrollToError() {
    const errorField = document.querySelector('.mat-error');
    errorField.scrollIntoView({ behavior: 'smooth' });
  }
}
