import { Component, OnInit, Inject } from '@angular/core';
import { Shift } from 'src/models/Shift';
import { ShiftsService } from 'src/app/services/shifts.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-delete-shift',
  templateUrl: './delete-shift.component.html',
  styleUrls: ['./delete-shift.component.scss']
})
export class DeleteShiftComponent {
  errorMessage = '';

  constructor(
    private service: ShiftsService,
    private matDialogRef: MatDialogRef<DeleteShiftComponent>,
    @Inject(MAT_DIALOG_DATA) public item: Shift
  ) {
  }

  delete() {
    this.service.delete(this.item.id).subscribe(() => {
      this.matDialogRef.close(true);
    }, error => {
      this.errorMessage = 'Неможливо видалити зміну, у якій є працівники. Спершу призначте працівникам іншу зміну або звільніть їх.';
    });
  }
}

