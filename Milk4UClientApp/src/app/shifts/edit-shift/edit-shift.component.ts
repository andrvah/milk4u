import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators, FormControl } from '@angular/forms';
import { Shift } from 'src/models/Shift';
import { Employee } from 'src/models/Employee';
import { ShiftSchedule } from 'src/models/ShiftSchedule';
import { ShiftsService } from 'src/app/services/shifts.service';
import { EmployeeService } from 'src/app/services/employee.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AddShiftComponent } from '../add-shift/add-shift.component';
import { Schedule } from 'src/models/Schedule';

@Component({
  selector: 'app-edit-shift',
  templateUrl: './edit-shift.component.html',
  styleUrls: ['./edit-shift.component.scss']
})
export class EditShiftComponent implements OnInit {
  addShiftForm: FormGroup;
  errorMessage = '';
  shift: Shift;
  employee: Employee[];
  employeeStrings = [];
  shiftsSchedule: ShiftSchedule[];
  currentEmployeeStr = [];
  currentDateStr = [];

  constructor(
    private formBuilder: FormBuilder,
    private service: ShiftsService,
    private employeeService: EmployeeService,
    private matDialogRef: MatDialogRef<AddShiftComponent>,
    @Inject(MAT_DIALOG_DATA) public item: Shift
    ) {
  }

  ngOnInit() {
    this.addShiftForm = new FormGroup({
      employee: new FormArray([]),
      shiftsSchedule: new FormArray([])
    });

    this.employeeService.get().subscribe( result => {
      result.forEach(element => {
        this.employeeStrings.push(`${element.id}: ${element.contact.firstName} ${element.contact.lastName} - ${element.position}`);
      });
    });

    this.item.shiftsSchedule.forEach(date => {
      this.addDate(date.date.split(':')[0]);
      this.currentDateStr.push(date.date.split(':')[0]);
    });

    this.item.employee.forEach(employee => {
      this.addEmployee(employee);
      this.currentEmployeeStr.push(`${employee.id}: ${employee.contact.firstName} ${employee.contact.lastName} - ${employee.position}`)
    });
  }

  createVacancy() {
    let empId;
    let formGroup;
    let index;

    this.shift = {
      id: this.item.id,
      employee: [],
      shiftsSchedule: new Array<Schedule>()
    };

    const employeeArray = (this.addShiftForm.get('employee') as FormArray);
    for (let i = 0; i < employeeArray.length; i++) {
      formGroup = employeeArray.at(i);
      index = formGroup.get('employeeId').value.toString().indexOf(':');
      if (index !== -1) {
        empId = formGroup.get('employeeId').value.slice(0, index);
      }
      const emp: Employee = {
        id: +formGroup.get('employeeId').value
      };
      if (empId !== undefined) {
        emp.id = +empId;
      }
      this.shift.employee.push(emp);
    }

    const scheduleArray = (this.addShiftForm.get('shiftsSchedule') as FormArray);
    for (let i = 0; i < scheduleArray.length; i++) {
      const value = scheduleArray.at(i).value;
      if (value.length === undefined) {
        this.shift.shiftsSchedule.push({date: value.toJSON()});
      } else {
        this.shift.shiftsSchedule.push({date: value});
      }
    }

    console.log(this.addShiftForm);
    if (this.addShiftForm.valid) {
      this.service.edit(this.shift).subscribe(response => {
        this.matDialogRef.close();
      }, error => {
        console.log(error);
        this.errorMessage = error.message;
        this.scrollToError();
      });
    } else {
      console.log(this.addShiftForm);
      this.errorMessage = 'Будь ласка, заповніть всі необхідні поля!';
      this.scrollToError();
    }
  }

  addEmployee(employee: Employee): void {
    const control = new FormGroup({
      employeeId: new FormControl(employee === null ? null : employee.id),
    });
    (this.addShiftForm.get('employee') as FormArray).push(control);
  }

  addDate(date): void {
    const control = new FormControl(date, Validators.required);
    (this.addShiftForm.get('shiftsSchedule') as FormArray).push(control);
  }

  deleteEmployee(index: number) {
    (this.addShiftForm.get('employee') as FormArray).removeAt(index);
  }

  deleteDate(index: number) {
    (this.addShiftForm.get('shiftsSchedule') as FormArray).removeAt(index);
  }

  scrollToError() {
    const errorField = document.querySelector('.mat-error');
    errorField.scrollIntoView({ behavior: 'smooth' });
  }
}
