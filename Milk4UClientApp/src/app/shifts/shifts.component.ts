import { Component, OnInit, EventEmitter, ViewChild } from '@angular/core';
import { Shift } from 'src/models/Shift';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatDialogConfig } from '@angular/material';
import { ProductService } from '../services/product.service';
import { Product } from 'src/models/Product';
import { AddProductComponent } from '../products/add-product/add-product.component';
import { DeleteProductComponent } from '../products/delete-product/delete-product.component';
import { EditProductComponent } from '../products/edit-product/edit-product.component';
import { ShiftsService } from '../services/shifts.service';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { EditShiftComponent } from './edit-shift/edit-shift.component';
import { DeleteShiftComponent } from './delete-shift/delete-shift.component';
import { AddShiftComponent } from './add-shift/add-shift.component';
import { FireEmployeeComponent } from '../employee/fire-employee/fire-employee.component';

@Component({
  selector: 'app-shifts',
  templateUrl: './shifts.component.html',
  styleUrls: ['./shifts.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', maxHeight: '0' })),
      state('expanded', style({ height: '*', paddingBottom: '10px' })),
      transition('expanded <=> collapsed', animate('500ms cubic-bezier(0.4, 0.0, 0.2, 1)'))
    ])
  ]
})
export class ShiftsComponent implements OnInit {
  displayedColumns: string[] = [ 'id', 'workersNumber', 'date', 'actions' ];
  dataSource: MatTableDataSource<Shift>;
  expandedElement: Shift | null;
  itemsChanged: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private dialog: MatDialog,
    private  service: ShiftsService
  ) {
  }

  ngOnInit() {
    this.itemsChanged.subscribe(() => {
      this.expandedElement = null;
      this.service.get().subscribe(resp => {
        resp.forEach(product => {
          product.date = `${product.shiftsSchedule.length > 0 ?
            product.shiftsSchedule[0].date.toString().slice(0, 10) :
            'Зміні не присвоєна дата'}`;
          if (product.shiftsSchedule.length > 0) {
            product.shiftsSchedule.forEach(schedule => {
              schedule.date = schedule.date.slice(0, 10);
            });
          }
        });
        this.dataSource = new MatTableDataSource<Shift>(resp);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
    });

    this.itemsChanged.emit();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  add() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(AddShiftComponent);
    dialogRef.afterClosed().subscribe(result => {
      this.itemsChanged.emit();
    });
  }

  delete(item) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.data = item;

    const dialogRef = this.dialog.open(DeleteShiftComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      this.itemsChanged.emit();
    });
  }

  edit(item) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.data = item;

    const dialogRef = this.dialog.open(EditShiftComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      this.itemsChanged.emit();
    });
  }
}
