import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from 'src/models/User';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {
  addUserForm: FormGroup;
  errorMessage = '';
  successMessage = '';
  user: User;

  constructor(private formBuilder: FormBuilder,
              private service: AuthService) { }

  ngOnInit() {
    this.addUserForm = this.formBuilder.group({
      userName: [ null, [ Validators.required, Validators.maxLength(50) ] ],
      password: [ null, [ Validators.required, Validators.maxLength(50) ] ],
      role: [ null, [ Validators.required, Validators.maxLength(50) ] ]
    });
  }
  createVacancy() {
    this.user = {
      userName: this.addUserForm.get('userName').value,
      role: +this.addUserForm.get('role').value,
      password: this.addUserForm.get('password').value
    };

    if (this.addUserForm.valid) {
      this.service.createUser(this.user).subscribe(response => {
        this.successMessage = `Користувача ${response.userName} додано`;
      }, error => {
        console.log(error);
        this.errorMessage = error.message;
        this.scrollToError();
      });
    } else {
      console.log(this.addUserForm);
      this.errorMessage = 'Будь ласка, заповніть всі необхідні поля!';
      this.scrollToError();
    }
  }

  scrollToError() {
    const errorField = document.querySelector('.mat-error');
    errorField.scrollIntoView({ behavior: 'smooth' });
  }
}

