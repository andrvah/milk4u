import { Component, OnInit, EventEmitter, ViewChild } from '@angular/core';
import { Essential } from 'src/models/Essentials';
import { EssentialsService } from '../services/essentials.service';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatDialogConfig } from '@angular/material';
import { AddEssentialsComponent } from './add-essentials/add-essentials.component';
import { DeleteEssentialsComponent } from './delete-essentials/delete-essentials.component';

@Component({
  selector: 'app-essentials',
  templateUrl: './essentials.component.html',
  styleUrls: ['./essentials.component.scss']
})
export class EssentialsComponent implements OnInit {
  displayedColumns: string[] = [ 'id', 'name', 'actions' ];
  dataSource: MatTableDataSource<Essential>;
  itemsChanged: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private dialog: MatDialog,
    private service: EssentialsService
  ) {
  }

  ngOnInit() {
    this.itemsChanged.subscribe(() => {
      this.service.get().subscribe(resp => {
        this.dataSource = new MatTableDataSource<Essential>(resp);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
    });

    this.itemsChanged.emit();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  add() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(AddEssentialsComponent);
    dialogRef.afterClosed().subscribe(result => {
      this.itemsChanged.emit();
    });
  }

  delete(item) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.data = item;

    const dialogRef = this.dialog.open(DeleteEssentialsComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      this.itemsChanged.emit();
    });
  }
}
