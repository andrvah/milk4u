import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEssentialsComponent } from './add-essentials.component';

describe('AddEssentialsComponent', () => {
  let component: AddEssentialsComponent;
  let fixture: ComponentFixture<AddEssentialsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEssentialsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEssentialsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
