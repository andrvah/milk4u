import { Component, OnInit } from '@angular/core';
import { EssentialsService } from 'src/app/services/essentials.service';
import { Essential } from 'src/models/Essentials';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-add-essentials',
  templateUrl: './add-essentials.component.html',
  styleUrls: ['./add-essentials.component.scss']
})
export class AddEssentialsComponent implements OnInit {
  addEssentialForm: FormGroup;
  errorMessage = '';
  essential: Essential;

  constructor(
    private formBuilder: FormBuilder,
    private service: EssentialsService,
    private matDialogRef: MatDialogRef<AddEssentialsComponent>
  ) {
  }

  ngOnInit() {
    this.addEssentialForm = this.formBuilder.group({
      name: [ null, [ Validators.required, Validators.maxLength(50) ] ]
    });
  }

  createVacancy() {
    this.essential = {
      name: this.addEssentialForm.get('name').value
    };

    if (this.addEssentialForm.valid) {
      this.service.add(this.essential).subscribe(response => {
        this.matDialogRef.close();
      }, error => {
        console.log(error);
        this.errorMessage = error.message;
        this.scrollToError();
      });
    } else {
      console.log(this.addEssentialForm);
      this.errorMessage = 'Будь ласка, заповніть всі необхідні поля!';
      this.scrollToError();
    }
  }

  scrollToError() {
    const errorField = document.querySelector('.mat-error');
    errorField.scrollIntoView({ behavior: 'smooth' });
  }
}
