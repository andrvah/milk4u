import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteEssentialsComponent } from './delete-essentials.component';

describe('DeleteEssentialsComponent', () => {
  let component: DeleteEssentialsComponent;
  let fixture: ComponentFixture<DeleteEssentialsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteEssentialsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteEssentialsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
