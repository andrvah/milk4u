import { Component, OnInit, Inject } from '@angular/core';
import { EssentialsService } from 'src/app/services/essentials.service';
import { Essential } from 'src/models/Essentials';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-delete-essentials',
  templateUrl: './delete-essentials.component.html',
  styleUrls: ['./delete-essentials.component.scss']
})
export class DeleteEssentialsComponent {
  errorMessage = '';

  constructor(
    private service: EssentialsService,
    private matDialogRef: MatDialogRef<DeleteEssentialsComponent>,
    @Inject(MAT_DIALOG_DATA) public item: Essential
  ) {
  }

  delete() {
    this.service.delete(this.item.id).subscribe(() => {
      this.matDialogRef.close(true);
    }, error => {
      this.errorMessage = error.statusText;
    });
  }
}
