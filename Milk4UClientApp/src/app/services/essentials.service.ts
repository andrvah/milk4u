import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Essential } from 'src/models/Essentials';
import { Observable } from 'rxjs';
import { EssentialOrder } from 'src/models/EssentialOrder';
import { Provider } from 'src/models/Provider';

@Injectable({
  providedIn: 'root'
})
export class EssentialsService {
  host = environment.url + '/Essentials';

  constructor(private http: HttpClient) {
  }

  get(): Observable<Essential[]> {
    return this.http.get<Essential[]>(this.host);
  }

  add(item: Essential) {
    return this.http.post(this.host, item);
  }

  delete(id: number) {
    return this.http.delete(`${this.host}/${id}`);
  }

  edit(item: Essential) {
    return this.http.put(this.host, item);
  }

  order(item: EssentialOrder) {
    return this.http.post(this.host + '/order', item);
  }

  getProviders(): Observable<Provider[]> {
    return this.http.get<Provider[]>(`${this.host}/providers`);
  }
}
