import { Injectable } from '@angular/core';
import { Order } from 'src/models/Order';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Return } from 'src/models/Return';
import { Customer } from 'src/models/Customer';
import { Saler } from 'src/models/Saler';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {
  host = environment.url + '/orders';

  constructor(private http: HttpClient) {
  }

  get(): Observable<Order[]> {
    return this.http.get<Order[]>(this.host);
  }

  getCustomers() {
    return this.http.get<Customer[]>(this.host + '/customers');
  }

  getSalers() {
    return this.http.get<Saler[]>(`${this.host}/salers`);
  }

  add(item: Order) {
    return this.http.post(this.host, item);
  }

  delete(id: number) {
    return this.http.delete(`${this.host}/${id}`);
  }

  returnOrder(item: Return) {
    return this.http.post(this.host + '/return', item);
  }
}
