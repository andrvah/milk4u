import { Injectable } from '@angular/core';
import { Revenue } from 'src/models/Revenue';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RevenueService {
  host = environment.url + '/revenue';

  constructor(private http: HttpClient) {
  }

  get(): Observable<Revenue[]> {
    return this.http.get<Revenue[]>(this.host);
  }

  add(item: Revenue) {
    let parameters = new HttpParams();
    parameters = parameters.append('startDate', item.startDate);
    parameters = parameters.append('endDate', item.endDate);
    return this.http.post(this.host, item, { params: parameters });
  }

  delete(id: number) {
    return this.http.delete(`${this.host}/${id}`);
  }
}
