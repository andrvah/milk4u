import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Shift } from 'src/models/Shift';
import { ShiftSchedule } from 'src/models/ShiftSchedule';

@Injectable({
  providedIn: 'root'
})
export class ShiftsService {

  host = environment.url + '/Shifts';

  constructor(private http: HttpClient) {
  }

  get() {
    return this.http.get<Shift[]>(this.host);
  }

  add(item: Shift) {
    return this.http.post(this.host, item);
  }

  addSchedule(item: ShiftSchedule) {
    return this.http.post(this.host, item);
  }

  edit(item: Shift) {
    return this.http.put(this.host, item);
  }

  delete(id: number) {
    return this.http.delete(`${this.host}/${id}`);
  }
}
