import { Injectable } from '@angular/core';
import { Warehouse } from 'src/models/Warehouse';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WarehouseService {

  host = environment.url + '/Warehouse';

  constructor(private http: HttpClient) {
  }

  get(): Observable<Warehouse[]> {
    return this.http.get<Warehouse[]>(this.host);
  }

  delete() {
    return this.http.delete(this.host);
  }
}
