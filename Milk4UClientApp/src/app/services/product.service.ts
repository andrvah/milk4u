import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Product } from '../../models/Product';
import { Package } from 'src/models/Package';
import { Nutritional } from 'src/models/Nutritional';
import { Expiration } from 'src/models/Expiration';
import { Warehouse } from 'src/models/Warehouse';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  host = environment.url + '/products';

  constructor(private http: HttpClient) {
  }

  get(): Observable<Product[]> {
    return this.http.get<Product[]>(this.host);
  }

  add(item: Product) {
    return this.http.post(this.host, item);
  }

  delete(id: number) {
    return this.http.delete(`${this.host}/${id}`);
  }

  edit(item: Product) {
    return this.http.put(this.host, item);
  }

  getPackages(): Observable<Package[]> {
    return this.http.get<Package[]>(`${this.host}/packages`);
  }

  getNutritionals(): Observable<Nutritional[]> {
    return this.http.get<Nutritional[]>(`${this.host}/nutritionals`);
  }

  getExpirations(): Observable<Expiration[]> {
    return this.http.get<Expiration[]>(`${this.host}/expirations`);
  }

  produce(productId: number, amount: number): Observable<Warehouse> {
    let parameters = new HttpParams();
    parameters = parameters.append('productId', productId.toString());
    parameters = parameters.append('amount', amount.toString());
    return this.http.post<Warehouse>(this.host + '/produce', null, {
      params: parameters,
    });
  }
}
