import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  host = environment.url + '/Storage';

  constructor(private http: HttpClient) {
  }

  get(): Observable<Storage[]> {
    return this.http.get<Storage[]>(this.host);
  }

  delete() {
    return this.http.delete(this.host);
  }
}
