import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Employee } from 'src/models/Employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  host = environment.url + '/Employee';

  constructor(private http: HttpClient) {
  }

  get() {
    return this.http.get<Employee[]>(this.host);
  }

  add(item: Employee) {
    return this.http.post(this.host, item);
  }

  delete(id: number) {
    return this.http.delete(`${this.host}/${id}`);
  }

  edit(item: Employee) {
    return this.http.put(this.host, item);
  }
}
