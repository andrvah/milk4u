import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { User } from 'src/models/User';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  host = environment.url + '/Auth';
  userChanged = new Subject<User>();
  loggedIn = false;

  constructor(private http: HttpClient) {
  }

  createUser(user: User): Observable<User> {
    return this.http.post<User>(this.host, user);
  }

  login(user: User): Observable<User> {
    return this.http.post<User>(this.host + '/login', user);
  }
}
