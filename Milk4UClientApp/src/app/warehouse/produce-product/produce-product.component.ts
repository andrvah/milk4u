import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { ProductService } from 'src/app/services/product.service';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-produce-product',
  templateUrl: './produce-product.component.html',
  styleUrls: ['./produce-product.component.scss']
})
export class ProduceProductComponent implements OnInit {
  produceProductForm: FormGroup;
  errorMessage = '';
  productsList: string[] = [];

  constructor(
    private service: ProductService,
    private matDialogRef: MatDialogRef<ProduceProductComponent>
  ) {}

  ngOnInit() {
    this.produceProductForm = new FormGroup({
      productId: new FormControl(null, [
        Validators.required,
        Validators.maxLength(100),
      ]),
      amount: new FormControl(null, [
        Validators.required,
        Validators.max(1000),
      ])
    });

    this.service.get().subscribe(result => {
      result.forEach(element => {
        this.productsList.push(`${element.id}: ${element.name}`);
      });
    });
  }

  createVacancy() {
    let value = this.produceProductForm.get('productId').value;
    let index = value.indexOf(':');
    let prdId = value.slice(0, index);
    let amount = this.produceProductForm.get('amount').value;

    if (this.produceProductForm.valid) {
      this.service.produce(+prdId, +amount).subscribe(
        (response) => {
          this.matDialogRef.close();
        },
        (error) => {
          console.log(error);
          if (error.status === 404) {
            this.errorMessage = error.error;
            this.scrollToError();
          } else {
            this.errorMessage = 'Неможливо виготовити продукт через брак складників';
            this.scrollToError();
          }
        }
      );
    } else {
      console.log(this.produceProductForm);
      this.errorMessage = 'Будь ласка, заповніть всі необхідні поля!';
      this.scrollToError();
    }
  }

  scrollToError() {
    const errorField = document.querySelector('.mat-error');
    errorField.scrollIntoView({ behavior: 'smooth' });
  }
}
