import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProduceProductComponent } from './produce-product.component';

describe('ProduceProductComponent', () => {
  let component: ProduceProductComponent;
  let fixture: ComponentFixture<ProduceProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProduceProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProduceProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
