import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CleanWarehouseComponent } from './clean-warehouse.component';

describe('CleanWarehouseComponent', () => {
  let component: CleanWarehouseComponent;
  let fixture: ComponentFixture<CleanWarehouseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CleanWarehouseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CleanWarehouseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
