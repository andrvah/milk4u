import { Component, OnInit } from '@angular/core';
import { WarehouseService } from 'src/app/services/warehouse.service';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-clean-warehouse',
  templateUrl: './clean-warehouse.component.html',
  styleUrls: ['./clean-warehouse.component.scss']
})
export class CleanWarehouseComponent {
  errorMessage = '';

  constructor(
    private service: WarehouseService,
    private matDialogRef: MatDialogRef<CleanWarehouseComponent>,
  ) {
  }

  delete() {
    this.service.delete().subscribe(() => {
      this.matDialogRef.close(true);
    }, error => {
      this.errorMessage = error.statusText;
    });
  }
}
