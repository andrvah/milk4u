import { Component, OnInit, EventEmitter, ViewChild } from '@angular/core';
import { Warehouse } from 'src/models/Warehouse';
import { WarehouseService } from '../services/warehouse.service';
import { ProduceProductComponent } from './produce-product/produce-product.component';
import { CleanWarehouseComponent } from './clean-warehouse/clean-warehouse.component';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatDialogConfig } from '@angular/material';

@Component({
  selector: 'app-warehouse',
  templateUrl: './warehouse.component.html',
  styleUrls: ['./warehouse.component.scss']
})
export class WarehouseComponent implements OnInit {
  displayedColumns: string[] = [ 'id', 'product', 'amount', 'date', 'shift'];
  dataSource: MatTableDataSource<Warehouse>;
  itemsChanged: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private dialog: MatDialog,
    private service: WarehouseService
  ) {
  }

  ngOnInit() {
    this.itemsChanged.subscribe(() => {
      this.service.get().subscribe(resp => {
        resp.forEach((war) => {
          if (war.date !== null) {
            war.date = war.date.slice(0, 10);
          }
          war.productName = war.product.name;
        });
        this.dataSource = new MatTableDataSource<Warehouse>(resp);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
    });

    this.itemsChanged.emit();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  produce() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(ProduceProductComponent);
    dialogRef.afterClosed().subscribe(result => {
      this.itemsChanged.emit();
    });
  }

  clear() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(CleanWarehouseComponent);
    dialogRef.afterClosed().subscribe(result => {
      this.itemsChanged.emit();
    });
  }
}

