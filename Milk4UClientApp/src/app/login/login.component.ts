import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from 'src/models/User';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  addUserForm: FormGroup;
  errorMessage = '';
  user: User;

  constructor(private formBuilder: FormBuilder,
              private service: AuthService,
              private router: Router) { }

  ngOnInit() {
    this.addUserForm = this.formBuilder.group({
      userName: [ null, [ Validators.required, Validators.maxLength(50) ] ],
      password: [ null, [ Validators.required, Validators.maxLength(50) ] ],
    });
  }
  createVacancy() {
    this.user = {
      userName: this.addUserForm.get('userName').value,
      password: this.addUserForm.get('password').value
    };

    if (this.addUserForm.valid) {
      this.service.login(this.user).subscribe(response => {
        this.service.userChanged.next(response);
        this.router.navigateByUrl('/products');
        this.service.loggedIn = true;
      }, error => {
        console.log(error);
        this.errorMessage = 'У доступі до системи відмовлено. Перевірте дані і спробуйте знову';
        this.scrollToError();
      });
    } else {
      console.log(this.addUserForm);
      this.errorMessage = 'Будь ласка, заповніть всі необхідні поля!';
      this.scrollToError();
    }
  }

  scrollToError() {
    const errorField = document.querySelector('.mat-error');
    errorField.scrollIntoView({ behavior: 'smooth' });
  }
}
