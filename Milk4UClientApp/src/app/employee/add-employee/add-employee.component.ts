import { Component, OnInit } from '@angular/core';
import { Employee } from 'src/models/Employee';
import { EmployeeService } from 'src/app/services/employee.service';
import { Validators, FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.scss']
})
export class AddEmployeeComponent implements OnInit {
  addEmployeeForm: FormGroup;
  errorMessage = '';
  employee: Employee;

  constructor(
    private formBuilder: FormBuilder,
    private service: EmployeeService,
    private matDialogRef: MatDialogRef<AddEmployeeComponent>
  ) {
  }

  ngOnInit() {
    this.addEmployeeForm = this.formBuilder.group({
      position: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
      salary: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
      firstName: new FormControl(null, [
        Validators.required,
        Validators.maxLength(50),
      ]),
      lastName: new FormControl(null, [
        Validators.required,
        Validators.maxLength(50),
      ]),
      phoneNumber: new FormControl(null, [
        Validators.required,
        Validators.maxLength(50),
      ]),
      email: new FormControl(null, [
        Validators.required,
        Validators.maxLength(50),
      ])
    });
  }

  createVacancy() {
    this.employee = {
      position: this.addEmployeeForm.get('position').value,
      salary: +this.addEmployeeForm.get('salary').value,
      contact: {
        firstName: this.addEmployeeForm.get('firstName').value,
        lastName: this.addEmployeeForm.get('lastName').value,
        phoneNumber: this.addEmployeeForm.get('phoneNumber').value,
        eMail: this.addEmployeeForm.get('email').value
      }
    };

    if (this.addEmployeeForm.valid) {
      this.service.add(this.employee).subscribe(response => {
        this.matDialogRef.close();
      }, error => {
        console.log(error);
        this.errorMessage = error.message;
        this.scrollToError();
      });
    } else {
      console.log(this.addEmployeeForm);
      this.errorMessage = 'Будь ласка, заповніть всі необхідні поля!';
      this.scrollToError();
    }
  }

  scrollToError() {
    const errorField = document.querySelector('.mat-error');
    errorField.scrollIntoView({ behavior: 'smooth' });
  }
}

