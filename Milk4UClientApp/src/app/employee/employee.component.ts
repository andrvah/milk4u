import { Component, OnInit, EventEmitter, ViewChild } from '@angular/core';
import { Employee } from 'src/models/Employee';
import { EmployeeService } from '../services/employee.service';
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import { FireEmployeeComponent } from './fire-employee/fire-employee.component';
import { EditEmployeeComponent } from './edit-employee/edit-employee.component';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatDialogConfig } from '@angular/material';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', maxHeight: '0' })),
      state('expanded', style({ height: '100px' })),
      transition('expanded <=> collapsed', animate('500ms cubic-bezier(0.4, 0.0, 0.2, 1)'))
    ])
  ]
})
export class EmployeeComponent implements OnInit {

  displayedColumns: string[] = [ 'id', 'firstName', 'lastName', 'position', 'shiftId', 'salary', 'actions' ];
  dataSource: MatTableDataSource<Employee>;
  expandedElement: Employee | null;
  itemsChanged: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private dialog: MatDialog,
    private  service: EmployeeService
  ) {
  }

  ngOnInit() {
    this.itemsChanged.subscribe(() => {
      this.expandedElement = null;
      this.service.get().subscribe(resp => {
        resp.forEach(emp => {
          emp.firstName = emp.contact.firstName;
          emp.lastName = emp.contact.lastName;
        });
        this.dataSource = new MatTableDataSource<Employee>(resp);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
    });

    this.itemsChanged.emit();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  add() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(AddEmployeeComponent);
    dialogRef.afterClosed().subscribe(result => {
      this.itemsChanged.emit();
    });
  }

  delete(item) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.data = item;

    const dialogRef = this.dialog.open(FireEmployeeComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      this.itemsChanged.emit();
    });
  }

  edit(item) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.data = item;

    const dialogRef = this.dialog.open(EditEmployeeComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      this.itemsChanged.emit();
    });
  }
}
