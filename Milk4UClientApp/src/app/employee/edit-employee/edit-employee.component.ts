import { Component, OnInit, AfterViewInit, Inject } from '@angular/core';
import { Employee } from 'src/models/Employee';
import { EmployeeService } from 'src/app/services/employee.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-edit-employee',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.scss']
})
export class EditEmployeeComponent implements OnInit, AfterViewInit {
  editEmployeeForm: FormGroup;
  errorMessage = '';
  employee: Employee;

  constructor(
    private formBuilder: FormBuilder,
    private service: EmployeeService,
    private matDialogRef: MatDialogRef<EditEmployeeComponent>,
    @Inject(MAT_DIALOG_DATA) public item: Employee
  ) {
  }

  ngAfterViewInit(): void {
    this.editEmployeeForm.get('position').setValue(this.item.position);
    this.editEmployeeForm.get('salary').setValue(this.item.salary);
    this.editEmployeeForm.get('firstName').setValue(this.item.contact.firstName);
    this.editEmployeeForm.get('lastName').setValue(this.item.contact.lastName);
    this.editEmployeeForm.get('phoneNumber').setValue(this.item.contact.phoneNumber);
    this.editEmployeeForm.get('email').setValue(this.item.contact.eMail);
  }

  ngOnInit() {
    this.editEmployeeForm = new FormGroup({
      position: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
      salary: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
      firstName: new FormControl(null, [
        Validators.required,
        Validators.maxLength(50),
      ]),
      lastName: new FormControl(null, [
        Validators.required,
        Validators.maxLength(50),
      ]),
      phoneNumber: new FormControl(null, [
        Validators.required,
        Validators.maxLength(50),
      ]),
      email: new FormControl(null, [
        Validators.required,
        Validators.maxLength(50),
      ])
    });
  }

  createVacancy() {
    this.employee = {
      id: this.item.id,
      position: this.editEmployeeForm.get('position').value,
      salary: +this.editEmployeeForm.get('salary').value,
      shiftId: this.item.shiftId,
      contact: {
        id: this.item.contact.id,
        firstName: this.editEmployeeForm.get('firstName').value,
        lastName: this.editEmployeeForm.get('lastName').value,
        phoneNumber: this.editEmployeeForm.get('phoneNumber').value,
        eMail: this.editEmployeeForm.get('email').value
      }
    };

    if (this.editEmployeeForm.valid) {
      this.service.edit(this.employee).subscribe(response => {
        this.matDialogRef.close();
      }, error => {
        console.log(error);
        this.errorMessage = error.message;
        this.scrollToError();
      });
    } else {
      console.log(this.editEmployeeForm);
      this.errorMessage = 'Будь ласка, заповніть всі необхідні поля!';
      this.scrollToError();
    }
  }

  scrollToError() {
    const errorField = document.querySelector('.mat-error');
    errorField.scrollIntoView({ behavior: 'smooth' });
  }
}
