import { Component, OnInit, Inject } from '@angular/core';
import { EmployeeService } from 'src/app/services/employee.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, FormGroup } from '@angular/forms';
import { Employee } from 'src/models/Employee';

@Component({
  selector: 'app-fire-employee',
  templateUrl: './fire-employee.component.html',
  styleUrls: ['./fire-employee.component.scss']
})
export class FireEmployeeComponent  {
  errorMessage = '';

  constructor(
    private service: EmployeeService,
    private matDialogRef: MatDialogRef<FireEmployeeComponent>,
    @Inject(MAT_DIALOG_DATA) public item: Employee
  ) {
  }

  delete() {
    this.service.delete(this.item.id).subscribe(() => {
      this.matDialogRef.close(true);
    }, error => {
      this.errorMessage = error.statusText;
    });
  }
}
