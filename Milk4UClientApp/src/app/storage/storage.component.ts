import { Component, OnInit, EventEmitter, ViewChild } from '@angular/core';
import { StorageService } from '../services/storage.service';
import { OrderEssentialsComponent } from './order-essentials/order-essentials.component';
import { CleanStorageComponent } from './clean-storage/clean-storage.component';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatDialogConfig } from '@angular/material';

@Component({
  selector: 'app-storage',
  templateUrl: './storage.component.html',
  styleUrls: ['./storage.component.scss']
})
export class StorageComponent implements OnInit {
  displayedColumns: string[] = [ 'id', 'essential', 'weight', 'date'];
  dataSource: MatTableDataSource<Storage>;
  itemsChanged: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private dialog: MatDialog,
    private service: StorageService
  ) {
  }

  ngOnInit() {
    this.itemsChanged.subscribe(() => {
      this.service.get().subscribe(resp => {
        resp.forEach((stor) => {
          if (stor.date !== null) {
            stor.date = stor.date.slice(0, 10);
          }
          stor.essetialName = stor.essential.name;
        });
        this.dataSource = new MatTableDataSource<Storage>(resp);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
    });

    this.itemsChanged.emit();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  produce() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(OrderEssentialsComponent);
    dialogRef.afterClosed().subscribe(result => {
      this.itemsChanged.emit();
    });
  }

  clear() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(CleanStorageComponent);
    dialogRef.afterClosed().subscribe(result => {
      this.itemsChanged.emit();
    });
  }
}

