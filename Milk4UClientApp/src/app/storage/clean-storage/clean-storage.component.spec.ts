import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CleanStorageComponent } from './clean-storage.component';

describe('CleanStorageComponent', () => {
  let component: CleanStorageComponent;
  let fixture: ComponentFixture<CleanStorageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CleanStorageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CleanStorageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
