import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/services/storage.service';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-clean-storage',
  templateUrl: './clean-storage.component.html',
  styleUrls: ['./clean-storage.component.scss']
})
export class CleanStorageComponent {
  errorMessage = '';

  constructor(
    private service: StorageService,
    private matDialogRef: MatDialogRef<CleanStorageComponent>,
  ) {
  }

  delete() {
    this.service.delete().subscribe(() => {
      this.matDialogRef.close(true);
    }, error => {
      this.errorMessage = error.statusText;
    });
  }
}
