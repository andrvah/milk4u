import { Component, OnInit } from '@angular/core';
import { EssentialOrder } from 'src/models/EssentialOrder';
import { EssentialsService } from 'src/app/services/essentials.service';
import { Validators, FormGroup, FormBuilder, FormControl, FormArray } from '@angular/forms';
import { EssentialsEssentialOrders } from 'src/models/EssentialsEssentialOrders';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-order-essentials',
  templateUrl: './order-essentials.component.html',
  styleUrls: ['./order-essentials.component.scss']
})
export class OrderEssentialsComponent implements OnInit {
  addOrderForm: FormGroup;
  errorMessage = '';
  essOrder: EssentialOrder;
  providersList: string[] = [];
  essentialsList: string[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private service: EssentialsService,
    private matDialogRef: MatDialogRef<OrderEssentialsComponent>
  ) {}

  ngOnInit() {
    this.addOrderForm = new FormGroup({
      price: new FormControl(null, [
        Validators.required,
      ]),
      providerId: new FormControl(null, [
        Validators.maxLength(50),
      ]),
      rating: new FormControl(null, [
        Validators.maxLength(50),
      ]),
      firstName: new FormControl(null, [
        Validators.maxLength(50),
      ]),
      lastName: new FormControl(null, [
        Validators.maxLength(50),
      ]),
      phoneNumber: new FormControl(null, [
        Validators.maxLength(50),
      ]),
      email: new FormControl(null, [
        Validators.maxLength(50),
      ]),
      essentialsEssentialOrders: new FormArray([]),
    });

    this.service.get().subscribe( result => {
      result.forEach(element => {
        this.essentialsList.push(`${element.id}: ${element.name}`);
      });
    });

    this.service.getProviders().subscribe( result => {
      result.forEach(element => {
        this.providersList.push(`${element.id}: ${element.contact.firstName} ${element.contact.lastName}`);
      });
    });
  }

  addProduct(): void {
    const control = new FormGroup({
      essentialId: new FormControl(null, Validators.required),
      weight: new FormControl(null, Validators.required),
    });
    (this.addOrderForm.get('essentialsEssentialOrders') as FormArray).push(control);
  }

  createVacancy() {
    let providerId;
    let index;
    let value;

    value = this.addOrderForm.get('providerId').value;
    if (value === null) {
      this.addOrderForm.get('firstName').setValidators(Validators.required);
      this.addOrderForm.get('lastName').setValidators(Validators.required);
      this.addOrderForm.get('phoneNumber').setValidators(Validators.required);
      this.addOrderForm.get('email').setValidators(Validators.required);
      this.addOrderForm.get('rating').setValidators(Validators.required);
      this.addOrderForm.get('firstName').updateValueAndValidity();
      this.addOrderForm.get('lastName').updateValueAndValidity();
      this.addOrderForm.get('phoneNumber').updateValueAndValidity();
      this.addOrderForm.get('email').updateValueAndValidity();
      this.addOrderForm.get('rating').updateValueAndValidity();
    } else {
      index = value.indexOf(':');
      providerId = this.addOrderForm.get('providerId').value.slice(0, index);
    }

    this.essOrder = {
      price: this.addOrderForm.get('price').value,
      provider: {
        contact: {
          firstName: this.addOrderForm.get('firstName').value,
          lastName: this.addOrderForm.get('lastName').value,
          phoneNumber: this.addOrderForm.get('phoneNumber').value,
          eMail: this.addOrderForm.get('email').value
        },
        rating: +this.addOrderForm.get('rating').value
      },
      essentialsEssentialOrders: Array<EssentialsEssentialOrders>()
    };

    if (providerId !== undefined) {
      this.essOrder.provider.id = +providerId;
    }

    const productsArray = (this.addOrderForm.get('essentialsEssentialOrders') as FormArray);
    for (let i = 0; i < productsArray.length; i++) {
      value = productsArray.at(i).value;
      index = value.essentialId.indexOf(':');
      const essId = value.essentialId.slice(0, index);
      const essWeight = value.weight;
      this.essOrder.essentialsEssentialOrders.push({
        essential: {
          id: +essId,
        },
        weight: +essWeight
      });
    }

    if (this.addOrderForm.valid) {
      this.service.order(this.essOrder).subscribe(
        (response) => {
          this.matDialogRef.close();
        },
        (error) => {
          console.log(error);
          this.errorMessage = error.message;
          this.scrollToError();
        }
      );
    } else {
      console.log(this.addOrderForm);
      this.errorMessage = 'Будь ласка, заповніть всі необхідні поля!';
      this.scrollToError();
    }
  }

  scrollToError() {
    const errorField = document.querySelector('.mat-error');
    errorField.scrollIntoView({ behavior: 'smooth' });
  }

  deleteEssential(index: number) {
    (this.addOrderForm.get('essentialsEssentialOrders') as FormArray).removeAt(index);
  }
}

