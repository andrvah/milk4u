import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderEssentialsComponent } from './order-essentials.component';

describe('OrderEssentialsComponent', () => {
  let component: OrderEssentialsComponent;
  let fixture: ComponentFixture<OrderEssentialsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderEssentialsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderEssentialsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
