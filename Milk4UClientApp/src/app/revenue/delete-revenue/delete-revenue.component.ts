import { Component, OnInit, Inject } from '@angular/core';
import { RevenueService } from 'src/app/services/revenue.service';
import { Revenue } from 'src/models/Revenue';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-delete-revenue',
  templateUrl: './delete-revenue.component.html',
  styleUrls: ['./delete-revenue.component.scss']
})
export class DeleteRevenueComponent {
  errorMessage = '';

  constructor(
    private service: RevenueService,
    private matDialogRef: MatDialogRef<DeleteRevenueComponent>,
    @Inject(MAT_DIALOG_DATA) public item: Revenue
  ) {
  }

  delete() {
    this.service.delete(this.item.id).subscribe(() => {
      this.matDialogRef.close(true);
    }, error => {
      this.errorMessage = error.statusText;
    });
  }
}
