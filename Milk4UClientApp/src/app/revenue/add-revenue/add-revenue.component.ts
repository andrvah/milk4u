import { Component, OnInit } from '@angular/core';
import { Revenue } from 'src/models/Revenue';
import { RevenueService } from 'src/app/services/revenue.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-add-revenue',
  templateUrl: './add-revenue.component.html',
  styleUrls: ['./add-revenue.component.scss']
})
export class AddRevenueComponent implements OnInit {
  addRevenueForm: FormGroup;
  errorMessage = '';
  revenue: Revenue;

  constructor(
    private formBuilder: FormBuilder,
    private service: RevenueService,
    private matDialogRef: MatDialogRef<AddRevenueComponent>
  ) {
  }

  ngOnInit() {
    this.addRevenueForm = this.formBuilder.group({
      startDate: [ null, [ Validators.required, Validators.maxLength(50) ] ],
      endDate: [ null, [ Validators.required, Validators.max(50) ] ],
    });
  }

  createVacancy() {
    let val = this.addRevenueForm.get('startDate').value;
    this.revenue = {
      startDate: this.addRevenueForm.get('startDate').value.toJSON(),
      endDate: this.addRevenueForm.get('endDate').value.toJSON()
    };

    if (this.addRevenueForm.valid) {
      console.log(this.addRevenueForm);
      this.service.add(this.revenue).subscribe(response => {
        this.matDialogRef.close();
      }, error => {
        console.log(error);
        this.errorMessage = error.message;
        this.scrollToError();
      });
    } else {
      console.log(this.addRevenueForm);
      this.errorMessage = 'Будь ласка, заповніть всі необхідні поля!';
      this.scrollToError();
    }
  }

  scrollToError() {
    const errorField = document.querySelector('.mat-error');
    errorField.scrollIntoView({ behavior: 'smooth' });
  }
}
