import { Component, OnInit, EventEmitter, ViewChild } from '@angular/core';
import { Revenue } from 'src/models/Revenue';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatDialogConfig } from '@angular/material';
import { RevenueService } from '../services/revenue.service';
import { DeleteRevenueComponent } from './delete-revenue/delete-revenue.component';
import { AddRevenueComponent } from './add-revenue/add-revenue.component';

@Component({
  selector: 'app-revenue',
  templateUrl: './revenue.component.html',
  styleUrls: ['./revenue.component.scss']
})
export class RevenueComponent implements OnInit {
  displayedColumns: string[] = [ 'id', 'revenue', 'incomes', 'expenses', 'startDate', 'endDate', 'actions' ];
  dataSource: MatTableDataSource<Revenue>;
  itemsChanged: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private dialog: MatDialog,
    private service: RevenueService
  ) {
  }

  ngOnInit() {
    this.itemsChanged.subscribe(() => {
      this.service.get().subscribe(resp => {
        resp.forEach((rev) => {
          rev.revenue = Math.round((rev.incomes - rev.expenses) * 100) / 100;
          rev.startDate = rev.startDate.slice(0, 10);
          rev.endDate = rev.endDate.slice(0, 10);
        });
        this.dataSource = new MatTableDataSource<Revenue>(resp);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
    });

    this.itemsChanged.emit();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  add() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(AddRevenueComponent);
    dialogRef.afterClosed().subscribe(result => {
      this.itemsChanged.emit();
    });
  }

  delete(item) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.data = item;

    const dialogRef = this.dialog.open(DeleteRevenueComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      this.itemsChanged.emit();
    });
  }
}
