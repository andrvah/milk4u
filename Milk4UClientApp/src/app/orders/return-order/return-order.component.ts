import { Component, OnInit, Inject } from '@angular/core';
import { Return } from 'src/models/Return';
import { OrdersService } from 'src/app/services/orders.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Order } from 'src/models/Order';

@Component({
  selector: 'app-return-order',
  templateUrl: './return-order.component.html',
  styleUrls: ['./return-order.component.scss']
})
export class ReturnOrderComponent implements OnInit {
  returnOrderForm: FormGroup;
  errorMessage = '';
  return: Return;

  constructor(
    private formBuilder: FormBuilder,
    private service: OrdersService,
    private matDialogRef: MatDialogRef<ReturnOrderComponent>,
    @Inject(MAT_DIALOG_DATA) public item: Order
  ) {
  }

  ngOnInit() {
    this.returnOrderForm = this.formBuilder.group({
      reason: [ null, [ Validators.required, Validators.maxLength(50) ] ]
    });
  }

  createVacancy() {
    this.return = {
      reason: this.returnOrderForm.get('reason').value,
      orderId: this.item.id
    };

    if (this.returnOrderForm.valid) {
      this.service.returnOrder(this.return).subscribe(response => {
        this.matDialogRef.close();
      }, error => {
        console.log(error);
        this.errorMessage = error.message;
        this.scrollToError();
      });
    } else {
      console.log(this.returnOrderForm);
      this.errorMessage = 'Будь ласка, заповніть всі необхідні поля!';
      this.scrollToError();
    }
  }

  scrollToError() {
    const errorField = document.querySelector('.mat-error');
    errorField.scrollIntoView({ behavior: 'smooth' });
  }
}
