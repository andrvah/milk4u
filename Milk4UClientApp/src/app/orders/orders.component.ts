import { Component, OnInit, EventEmitter, ViewChild } from '@angular/core';
import { Order } from 'src/models/Order';
import { OrdersService } from '../services/orders.service';
import { AddOrderComponent } from './add-order/add-order.component';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatDialogConfig } from '@angular/material';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { ReturnOrderComponent } from './return-order/return-order.component';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', maxHeight: '0' })),
      state('expanded', style({ height: '110px' })),
      transition('expanded <=> collapsed', animate('500ms cubic-bezier(0.4, 0.0, 0.2, 1)'))
    ])
  ]
})
export class OrdersComponent implements OnInit {
  displayedColumns: string[] = [ 'id', 'price', 'date', 'customer', 'saler', 'return', 'actions' ];
  dataSource: MatTableDataSource<Order>;
  expandedElement: Order | null;
  itemsChanged: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private dialog: MatDialog,
    private service: OrdersService
  ) {
  }

  ngOnInit() {
    this.itemsChanged.subscribe(() => {
      this.expandedElement = null;
      this.service.get().subscribe(resp => {
        resp.forEach(order => {
          order.returned = order.returns.length !== 0;
          order.returns.forEach(ret => {
            ret.date = ret.date.slice(0, 10);
          });
          order.date = order.date.slice(0, 10);
          order.customerName = `${order.customer.contact.firstName} ${order.customer.contact.lastName}`;
          order.salerName = `${order.saler.contact.firstName} ${order.saler.contact.lastName}`;
        });
        this.dataSource = new MatTableDataSource<Order>(resp);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
    });

    this.itemsChanged.emit();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  add() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(AddOrderComponent);
    dialogRef.afterClosed().subscribe(result => {
      this.itemsChanged.emit();
    });
  }

  returnOrder(item) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.data = item;

    const dialogRef = this.dialog.open(ReturnOrderComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      this.itemsChanged.emit();
    });
  }
}

