import { Component, OnInit, Inject } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
  FormArray,
} from '@angular/forms';
import { Return } from 'src/models/Return';
import { OrdersService } from 'src/app/services/orders.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ReturnOrderComponent } from '../return-order/return-order.component';
import { Order } from 'src/models/Order';
import { ProductService } from 'src/app/services/product.service';
import { OrdersProducts } from 'src/models/OrdersProducts';

@Component({
  selector: 'app-add-order',
  templateUrl: './add-order.component.html',
  styleUrls: ['./add-order.component.scss'],
})
export class AddOrderComponent implements OnInit {
  addOrderForm: FormGroup;
  errorMessage = '';
  order: Order;
  customersList: string[] = [];
  salersList: string[] = [];
  productsList: string[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private service: OrdersService,
    private productService: ProductService,
    private matDialogRef: MatDialogRef<AddOrderComponent>
  ) {}

  ngOnInit() {
    this.addOrderForm = new FormGroup({
      customerId: new FormControl(null, [
        Validators.maxLength(50),
      ]),
      cFirstName: new FormControl(null, [
        Validators.maxLength(50),
      ]),
      cLastName: new FormControl(null, [
        Validators.maxLength(50),
      ]),
      cPhoneNumber: new FormControl(null, [
        Validators.maxLength(50),
      ]),
      cEmail: new FormControl(null, [
        Validators.maxLength(50),
      ]),
      discount: new FormControl(null, [
        Validators.maxLength(50),
      ]),
      salerId: new FormControl(null, [
        Validators.maxLength(50),
      ]),
      firstName: new FormControl(null, [
        Validators.maxLength(50),
      ]),
      lastName: new FormControl(null, [
        Validators.maxLength(50),
      ]),
      phoneNumber: new FormControl(null, [
        Validators.maxLength(50),
      ]),
      email: new FormControl(null, [
        Validators.maxLength(50),
      ]),
      ordersProducts: new FormArray([]),
    });

    this.service.getCustomers().subscribe( result => {
      result.forEach(element => {
        this.customersList.push(`${element.id}: ${element.contact.firstName} ${element.contact.lastName}`);
      });
    });

    this.service.getSalers().subscribe( result => {
      result.forEach(element => {
        this.salersList.push(`${element.id}: ${element.contact.firstName} ${element.contact.lastName}`);
      });
    });

    this.productService.get().subscribe(result => {
      result.forEach(element => {
        this.productsList.push(`${element.id}: ${element.name}`);
      });
    });
  }

  addProduct(): void {
    const control = new FormGroup({
      productId: new FormControl(null, Validators.required),
      amount: new FormControl(null, Validators.required),
    });
    (this.addOrderForm.get('ordersProducts') as FormArray).push(control);
  }

  createVacancy() {
    let cstId;
    let slrId;
    let index;

    let value = this.addOrderForm.get('customerId').value;
    if (value === null) {
      this.addOrderForm.get('cFirstName').setValidators(Validators.required);
      this.addOrderForm.get('cLastName').setValidators(Validators.required);
      this.addOrderForm.get('discount').setValidators(Validators.required);
      this.addOrderForm.get('cPhoneNumber').setValidators(Validators.required);
      this.addOrderForm.get('cEmail').setValidators(Validators.required);
      this.addOrderForm.get('cFirstName').updateValueAndValidity();
      this.addOrderForm.get('cLastName').updateValueAndValidity();
      this.addOrderForm.get('discount').updateValueAndValidity();
      this.addOrderForm.get('cPhoneNumber').updateValueAndValidity();
      this.addOrderForm.get('cEmail').updateValueAndValidity();
    } else {
      index = value.indexOf(':');
      cstId = this.addOrderForm.get('customerId').value.slice(0, index);
    }

    value = this.addOrderForm.get('salerId').value;
    if (value === null) {
      this.addOrderForm.get('firstName').setValidators(Validators.required);
      this.addOrderForm.get('lastName').setValidators(Validators.required);
      this.addOrderForm.get('phoneNumber').setValidators(Validators.required);
      this.addOrderForm.get('email').setValidators(Validators.required);
      this.addOrderForm.get('firstName').updateValueAndValidity();
      this.addOrderForm.get('lastName').updateValueAndValidity();
      this.addOrderForm.get('phoneNumber').updateValueAndValidity();
      this.addOrderForm.get('email').updateValueAndValidity();
    } else {
      index = value.indexOf(':');
      slrId = this.addOrderForm.get('salerId').value.slice(0, index);
    }

    this.order = {
      customer: {
        contact: {
          firstName: this.addOrderForm.get('cFirstName').value,
          lastName: this.addOrderForm.get('cLastName').value,
          phoneNumber: this.addOrderForm.get('cPhoneNumber').value,
          eMail: this.addOrderForm.get('cEmail').value
        },
        discount: +this.addOrderForm.get('discount').value
      },
      saler: {
        contact: {
          firstName: this.addOrderForm.get('firstName').value,
          lastName: this.addOrderForm.get('lastName').value,
          phoneNumber: this.addOrderForm.get('phoneNumber').value,
          eMail: this.addOrderForm.get('email').value
        }
      },
      ordersProducts: new Array<OrdersProducts>()
    };

    if (slrId !== undefined) {
      this.order.salerId = +slrId;
    }
    if (cstId !== undefined) {
      this.order.customerId = +cstId;
    }

    const productsArray = (this.addOrderForm.get('ordersProducts') as FormArray);
    for (let i = 0; i < productsArray.length; i++) {
      value = productsArray.at(i).value;
      index = value.productId.indexOf(':');
      const essId = value.productId.slice(0, index);
      const essWeight = value.amount;
      this.order.ordersProducts.push({
        productId: +essId,
        amount: +essWeight
      });
    }

    if (this.addOrderForm.valid) {
      this.service.add(this.order).subscribe(
        (response) => {
          this.matDialogRef.close();
        },
        (error) => {
          console.log(error);
          this.errorMessage = 'Неможливо сформувати замовлення через брак продуктів';
          this.scrollToError();
        }
      );
    } else {
      console.log(this.addOrderForm);
      this.errorMessage = 'Будь ласка, заповніть всі необхідні поля!';
      this.scrollToError();
    }
  }

  scrollToError() {
    const errorField = document.querySelector('.mat-error');
    errorField.scrollIntoView({ behavior: 'smooth' });
  }

  checkNull(value, type: 'number' | 'string') {
    if (value == null) {
      return null;
    }

    if (type === 'number') {
      return +value;
    }

    return value;
  }

  deleteEssential(index: number) {
    (this.addOrderForm.get('ordersProducts') as FormArray).removeAt(index);
  }
}

