import { Component, OnInit, Input } from '@angular/core';
import { routes } from '../routing.module';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { User } from 'src/models/User';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: [ './header.component.scss' ]
})
export class HeaderComponent implements OnInit {
  user: User;
  currentRoutes = [
    { path: 'essentials', name: 'Складники', roles: [0, 2]  },
    { path: 'orders', name: 'Замовлення', roles: [0, 1] },
    { path: 'products', name: 'Продукти', roles: [0, 1, 2]  },
    { path: 'revenue', name: 'Доходи', roles: [0, 1]  },
    { path: 'shifts', name: 'Зміни', roles: [0, 2]  },
    { path: 'employee', name: 'Працівники', roles: [0, 2] },
    { path: 'storage', name: 'Сховище', roles: [0, 2]  },
    { path: 'warehouse', name: 'Склад', roles: [0, 1, 2]  },
    { path: 'addUser', name: 'Додати користувача', roles: [0] },
  ];

  constructor(private router: Router, public authService: AuthService) {
  }

  ngOnInit() {
    this.user = {
      role: -1,
      userName: ''
    } ;

    this.authService.userChanged.subscribe(user => {
      this.user = user;
    });
  }

  logout() {
    this.authService.loggedIn = false;
    this.user.role = -1;
    this.user.userName = '';
    this.router.navigateByUrl('');
  }
}
