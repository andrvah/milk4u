import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

import { Product } from '../../../models/Product';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-delete-product',
  templateUrl: './delete-product.component.html',
  styleUrls: [ './delete-product.component.scss' ]
})
export class DeleteProductComponent {
  errorMessage = '';

  constructor(
    private service: ProductService,
    private matDialogRef: MatDialogRef<DeleteProductComponent>,
    @Inject(MAT_DIALOG_DATA) public item: Product
  ) {
  }

  delete() {
    this.service.delete(this.item.id).subscribe(() => {
      this.matDialogRef.close(true);
    }, error => {
      this.errorMessage = error.statusText;
    });
  }
}
