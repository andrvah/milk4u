import { Component, EventEmitter, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ProductService } from '../services/product.service';
import { Product } from '../../models/Product';
import { state, trigger, transition, style, animate } from '@angular/animations';
import { DeleteProductComponent } from './delete-product/delete-product.component';
import { AddProductComponent } from './add-product/add-product.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { AuthService } from '../services/auth.service';
import { User } from 'src/models/User';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: [ './products.component.scss' ],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', maxHeight: '0' })),
      state('expanded', style({ height: '100px' })),
      transition('expanded <=> collapsed', animate('500ms cubic-bezier(0.4, 0.0, 0.2, 1)'))
    ])
  ]
})
export class ProductsComponent implements OnInit {
  displayedColumns: string[] = [ 'id', 'name', 'price', 'expiration', 'nutritional', 'package', 'actions' ];
  dataSource: MatTableDataSource<Product>;
  expandedElement: Product | null;
  itemsChanged: EventEmitter<any> = new EventEmitter<any>();
  user: User;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private dialog: MatDialog,
    private  service: ProductService,
    public authService: AuthService
  ) {
  }

  ngOnInit() {
    this.itemsChanged.subscribe(() => {
      this.expandedElement = null;
      this.service.get().subscribe(resp => {
        resp.forEach(product => {
          product.caloriesAmount = `${product.nutritional.calories} ккал`;
          product.expirationDays = `${product.expiration.daysAmount} днів`;
          product.packageVolume = `${product.package.volume} кг`;
          product.productsEssentals.forEach(essential => {
            essential.weight = Math.round(essential.weight * 100) / 100;
          });
        });
        this.dataSource = new MatTableDataSource<Product>(resp);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
    });

    this.itemsChanged.emit();

    this.authService.userChanged.subscribe(user => {
      this.user = user;
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  add() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(AddProductComponent);
    dialogRef.afterClosed().subscribe(result => {
      this.itemsChanged.emit();
    });
  }

  delete(item) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.data = item;

    const dialogRef = this.dialog.open(DeleteProductComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      this.itemsChanged.emit();
    });
  }

  edit(item) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.data = item;

    const dialogRef = this.dialog.open(EditProductComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      this.itemsChanged.emit();
    });
  }
}
