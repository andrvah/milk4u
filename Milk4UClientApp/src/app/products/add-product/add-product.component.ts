import { MatDialogRef } from '@angular/material';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';

import { Product } from '../../../models/Product';
import { ProductService } from '../../services/product.service';
import { Expiration } from 'src/models/Expiration';
import { Nutritional } from 'src/models/Nutritional';
import { Package } from 'src/models/Package';
import { EssentialsService } from 'src/app/services/essentials.service';
import { EssentialProduct } from 'src/models/EssentialProduct';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: [ './add-product.component.scss' ]
})
export class AddProductComponent implements OnInit {
  addProductForm: FormGroup;
  errorMessage = '';
  product: Product;
  expirations: Expiration[];
  expirationsStrings = new Array<string>();
  nutritionals: Nutritional[];
  nutritionalsStrings = new Array<string>();
  packages: Package[];
  packagesStrings = new Array<string>();
  essentialsStrings = new Array<string>();

  constructor(
    private formBuilder: FormBuilder,
    private service: ProductService,
    private essService: EssentialsService,
    private matDialogRef: MatDialogRef<AddProductComponent>
  ) {
  }

  ngOnInit() {
    this.addProductForm = new FormGroup({
      name: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
      price: new FormControl(null, [ Validators.required, Validators.max(9999) ]),
      expirationId: new FormControl( null, [ ] ),
      temperature: new FormControl( null, [ Validators.max(99) ] ),
      daysAmount: new FormControl( null, [ Validators.max(100) ] ),
      nutritionalId: new FormControl( null, [ ] ),
      fats: new FormControl( null, [ Validators.max(100) ] ),
      proteins: new FormControl( null, [ Validators.max(100) ] ),
      carbohydrates: new FormControl( null, [ Validators.max(100) ] ),
      calories: new FormControl( null, [ Validators.max(9999) ] ),
      packageId: new FormControl( null, [ ] ),
      type: new FormControl(null, [ Validators.maxLength(20) ] ),
      volume: new FormControl( null, [ Validators.max(9999) ] ),
      essentials: new FormArray([]),
    });

    this.service.getExpirations().subscribe( result => {
      this.expirations = result;
      this.expirations.forEach(element => {
        this.expirationsStrings.push(`${element.id}: ${element.daysAmount} днів при температурі ${element.temperature}°C`);
      });
    });

    this.service.getNutritionals().subscribe( result => {
      this.nutritionals = result;
      this.nutritionals.forEach(element => {
        this.nutritionalsStrings.push(`${element.id}: ${element.calories} калорій,
                ${element.proteins} білків, ${element.fats} жирів, ${element.carbohydrates} вуглеводів`);
      });
    });

    this.service.getPackages().subscribe( result => {
      this.packages = result;
      this.packages.forEach(element => {
        this.packagesStrings.push(`${element.id}: ${element.type}, ${element.volume} л`);
      });
    });

    this.essService.get().subscribe(result => {
      result.forEach(element => {
        this.essentialsStrings.push(`${element.id}: ${element.name}`);
      });
    });
  }

  createVacancy() {
    let expId;
    let packId;
    let nutId;
    let index;

    let value = this.addProductForm.get('expirationId').value;
    if (value === null) {
      this.addProductForm.get('temperature').setValidators(Validators.required);
      this.addProductForm.get('daysAmount').setValidators(Validators.required);
      this.addProductForm.get('temperature').updateValueAndValidity();
      this.addProductForm.get('daysAmount').updateValueAndValidity();
    } else {
      index = value.indexOf(':');
      expId = this.addProductForm.get('expirationId').value.slice(0, index);
    }

    value = this.addProductForm.get('nutritionalId').value;
    if (value === null) {
      this.addProductForm.get('calories').setValidators(Validators.required);
      this.addProductForm.get('fats').setValidators(Validators.required);
      this.addProductForm.get('proteins').setValidators(Validators.required);
      this.addProductForm.get('carbohydrates').setValidators(Validators.required);
      this.addProductForm.get('calories').updateValueAndValidity();
      this.addProductForm.get('fats').updateValueAndValidity();
      this.addProductForm.get('proteins').updateValueAndValidity();
      this.addProductForm.get('carbohydrates').updateValueAndValidity();
    } else {
      index = value.indexOf(':');
      nutId = this.addProductForm.get('nutritionalId').value.slice(0, index);
    }

    value = this.addProductForm.get('packageId').value;
    if (value === null) {
      this.addProductForm.get('type').setValidators(Validators.required);
      this.addProductForm.get('volume').setValidators(Validators.required);
      this.addProductForm.get('type').updateValueAndValidity();
      this.addProductForm.get('volume').updateValueAndValidity();
    } else {
      index = value.indexOf(':');
      packId = this.addProductForm.get('packageId').value.slice(0, index);
    }

    this.product = {
      name: this.addProductForm.get('name').value,
      price: +this.addProductForm.get('price').value,
      expiration: {
        id: 0,
        temperature: +this.addProductForm.get('temperature').value + 11 - 11,
        daysAmount: +this.addProductForm.get('daysAmount').value + 7 - 4 - 3
      },
      package: {
        id: 0,
        type: this.addProductForm.get('type').value,
        volume: +this.addProductForm.get('volume').value
      },
      nutritional: {
        id: 0,
        calories: +this.addProductForm.get('calories').value,
        fats: +this.addProductForm.get('fats').value,
        proteins: +this.addProductForm.get('proteins').value,
        carbohydrates: +this.addProductForm.get('carbohydrates').value
      },
      productsEssentals: new Array<EssentialProduct>()
    };

    const essentialsArray = (this.addProductForm.get('essentials') as FormArray);
    for (let i = 0; i < essentialsArray.length; i++) {
      value = essentialsArray.at(i).value;
      index = value.id.indexOf(':');
      const essId = value.id.slice(0, index);
      const essWeight = value.weight;
      this.product.productsEssentals.push({
        essential: { id: +essId},
        weight: +essWeight / 1000
      });
    }

    if (expId !== undefined) {
      this.product.expiration.id = +expId;
    }
    if (nutId !== undefined) {
      this.product.nutritional.id = +nutId;
    }
    if (packId !== undefined) {
      this.product.package.id = +packId;
    }
    console.log(this.addProductForm);
    if (this.addProductForm.valid) {
      this.service.add(this.product).subscribe(response => {
        this.matDialogRef.close();
      }, error => {
        console.log(error);
        this.errorMessage = error.message;
        this.scrollToError();
      });
    } else {
      console.log(this.addProductForm);
      this.errorMessage = 'Будь ласка, заповніть всі необхідні поля!';
      this.scrollToError();
    }
  }

  addEssential(): void {
    const control = new FormGroup({
      id: new FormControl(null, Validators.required),
      weight: new FormControl(null, Validators.required)});
    (this.addProductForm.get('essentials') as FormArray).push(control);
    console.log(this.addProductForm);
  }

  deleteEssential(index: number) {
    (this.addProductForm.get('essentials') as FormArray).removeAt(index);
  }

  scrollToError() {
    const errorField = document.querySelector('.mat-error');
    errorField.scrollIntoView({ behavior: 'smooth' });
  }
}
