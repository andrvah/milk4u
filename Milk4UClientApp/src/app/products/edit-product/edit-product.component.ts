import { Component, OnInit, Inject, AfterViewChecked, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import { Product } from 'src/models/Product';
import { ProductService } from 'src/app/services/product.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AddProductComponent } from '../add-product/add-product.component';
import { Expiration } from 'src/models/Expiration';
import { Nutritional } from 'src/models/Nutritional';
import { Package } from 'src/models/Package';
import { EssentialsService } from 'src/app/services/essentials.service';
import { EssentialProduct } from 'src/models/EssentialProduct';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss']
})
export class EditProductComponent implements OnInit, AfterViewInit {
  editProductForm: FormGroup;
  errorMessage = '';
  product: Product;
  expirations: Expiration[];
  expirationsStrings = new Array<string>();
  nutritionals: Nutritional[];
  nutritionalsStrings = new Array<string>();
  packages: Package[];
  packagesStrings = new Array<string>();
  curentExpirationStr = '';
  curentNutritionalStr = '';
  curentPackageStr = '';
  curentessentialsStr = new Array<string>();
  essentialsStrings = new Array<string>();

  constructor(
    private formBuilder: FormBuilder,
    private service: ProductService,
    private essService: EssentialsService,
    private matDialogRef: MatDialogRef<AddProductComponent>,
    @Inject(MAT_DIALOG_DATA) public item: Product
  ) {
  }
  ngAfterViewInit(): void {
    this.editProductForm.get('name').setValue(this.item.name);
    this.editProductForm.get('price').setValue(this.item.price);
    this.editProductForm.get('temperature').setValue(this.item.expiration.temperature);
    this.editProductForm.get('daysAmount').setValue(this.item.expiration.daysAmount);
    this.editProductForm.get('fats').setValue(this.item.nutritional.fats);
    this.editProductForm.get('proteins').setValue(this.item.nutritional.proteins);
    this.editProductForm.get('carbohydrates').setValue(this.item.nutritional.carbohydrates);
    this.editProductForm.get('calories').setValue(this.item.nutritional.calories);
    this.editProductForm.get('type').setValue(this.item.package.type);
    this.editProductForm.get('volume').setValue(this.item.package.volume);
  }

  ngOnInit() {
    this.editProductForm = new FormGroup({
      name: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
      price: new FormControl(null, [ Validators.required, Validators.max(9999) ]),
      expirationId: new FormControl( null, [ ] ),
      temperature: new FormControl( null, [ Validators.max(99) ] ),
      daysAmount: new FormControl( null, [ Validators.max(100) ] ),
      nutritionalId: new FormControl( null, [ ] ),
      fats: new FormControl( null, [ Validators.max(100) ] ),
      proteins: new FormControl( null, [ Validators.max(100) ] ),
      carbohydrates: new FormControl( null, [ Validators.max(100) ] ),
      calories: new FormControl( null, [ Validators.max(9999) ] ),
      packageId: new FormControl( null, [ ] ),
      type: new FormControl(null, [ Validators.maxLength(20) ] ),
      volume: new FormControl( null, [ Validators.max(9999) ] ),
      essentials: new FormArray([]),
    });

    if (this.item.expiration) {
      this.curentExpirationStr =
        `${this.item.expiration.id}: ${this.item.expiration.daysAmount} днів при температурі ${this.item.expiration.temperature}°C`;
    }

    if (this.item.nutritional) {
      this.curentNutritionalStr =
        `${this.item.nutritional.id}: ${this.item.nutritional.calories} калорій, ${this.item.nutritional.proteins} білків,` +
        ` ${this.item.nutritional.fats} жирів, ${this.item.nutritional.carbohydrates} вуглеводів`;
    }

    if (this.item.package) {
      this.curentPackageStr = `${this.item.package.id}: ${this.item.package.type}, ${this.item.package.volume} л`;
    }

    this.item.productsEssentals.forEach(ess => {
      this.curentessentialsStr.push(`${ess.essential.id}: ${ess.essential.name}`);
      this.addEssential(`${ess.essential.id}: ${ess.essential.name}`, ess.weight * 1000);
    });

    this.essService.get().subscribe(result => {
      result.forEach(element => {
        this.essentialsStrings.push(`${element.id}: ${element.name}`);
      });
    });

    this.service.getExpirations().subscribe( result => {
      this.expirations = result;
      this.expirations.forEach(element => {
        this.expirationsStrings.push(`${element.id}: ${element.daysAmount} днів при температурі ${element.temperature}°C`);
      });
    });

    this.service.getNutritionals().subscribe( result => {
      this.nutritionals = result;
      this.nutritionals.forEach(element => {
        this.nutritionalsStrings.push(
          `${element.id}: ${element.calories} калорій, ${element.proteins} білків,` +
          ` ${element.fats} жирів, ${element.carbohydrates} вуглеводів`);
      });
    });

    this.service.getPackages().subscribe( result => {
      this.packages = result;
      this.packages.forEach(element => {
        this.packagesStrings.push(`${element.id}: ${element.type}, ${element.volume} л`);
      });
    });
  }

  createVacancy() {
    let expId;
    let packId;
    let nutId;
    let index;

    let value = this.editProductForm.get('expirationId').value;
    if (value === null) {
      this.editProductForm.get('temperature').setValidators(Validators.required);
      this.editProductForm.get('daysAmount').setValidators(Validators.required);
      this.editProductForm.get('temperature').updateValueAndValidity();
      this.editProductForm.get('daysAmount').updateValueAndValidity();
    } else {
      index = value.indexOf(':');
      expId = this.editProductForm.get('expirationId').value.slice(0, index);
    }

    value = this.editProductForm.get('nutritionalId').value;
    if (value === null) {
      this.editProductForm.get('calories').setValidators(Validators.required);
      this.editProductForm.get('fats').setValidators(Validators.required);
      this.editProductForm.get('proteins').setValidators(Validators.required);
      this.editProductForm.get('carbohydrates').setValidators(Validators.required);
      this.editProductForm.get('calories').updateValueAndValidity();
      this.editProductForm.get('fats').updateValueAndValidity();
      this.editProductForm.get('proteins').updateValueAndValidity();
      this.editProductForm.get('carbohydrates').updateValueAndValidity();
    } else {
      index = value.indexOf(':');
      nutId = this.editProductForm.get('nutritionalId').value.slice(0, index);
    }

    value = this.editProductForm.get('packageId').value;
    if (value === null) {
      this.editProductForm.get('type').setValidators(Validators.required);
      this.editProductForm.get('volume').setValidators(Validators.required);
      this.editProductForm.get('type').updateValueAndValidity();
      this.editProductForm.get('volume').updateValueAndValidity();
    } else {
      index = value.indexOf(':');
      packId = this.editProductForm.get('packageId').value.slice(0, index);
    }

    this.product = {
      id: +this.item.id,
      name: this.editProductForm.get('name').value,
      price: +this.editProductForm.get('price').value,
      expiration: {
        temperature: +this.editProductForm.get('temperature').value,
        daysAmount: +this.editProductForm.get('daysAmount').value
      },
      package: {
        type: this.editProductForm.get('type').value,
        volume: +this.editProductForm.get('volume').value
      },
      nutritional: {
        calories: +this.editProductForm.get('calories').value,
        fats: +this.editProductForm.get('fats').value,
        proteins: +this.editProductForm.get('proteins').value,
        carbohydrates: +this.editProductForm.get('carbohydrates').value
      },
      productsEssentals: new Array<EssentialProduct>()
    };

    const essentialsArray = (this.editProductForm.get('essentials') as FormArray);
    for (let i = 0; i < essentialsArray.length; i++) {
      essentialsArray.at(i).updateValueAndValidity();
      essentialsArray.at(i).updateValueAndValidity();
      value = essentialsArray.at(i).value;
      index = value.id.indexOf(':');
      const essId = value.id.slice(0, index);
      const essWeight = value.weight;
      this.product.productsEssentals.push({
        essential: { id: +essId},
        weight: +essWeight / 1000
      });
    }

    if (expId !== '') {
      if (expId === undefined) {
        this.product.expiration.id = this.item.expiration.id;
      } else {
        this.product.expiration.id = +expId;
      }
    }
    if (nutId !== '') {
      if (nutId === undefined) {
        this.product.nutritional.id = this.item.nutritional.id;
      } else {
        this.product.nutritional.id = +nutId;
      }
    }
    if (packId !== '') {
      if (packId === undefined) {
        this.product.package.id = this.item.package.id;
      } else {
        this.product.package.id = +packId;
      }
    }

    if (this.editProductForm.valid) {
      this.service.edit(this.product).subscribe(response => {
        this.matDialogRef.close();
      }, error => {
        console.log(error);
        this.errorMessage = error.message;
        this.scrollToError();
      });
    } else {
      console.log(this.editProductForm);
      this.errorMessage = 'Будь ласка, заповніть всі необхідні поля!';
      this.scrollToError();
    }
  }

  scrollToError() {
    const errorField = document.querySelector('.mat-error');
    errorField.scrollIntoView({ behavior: 'smooth' });
  }

  addEssential(id: string, weight: number): void {
    const control = new FormGroup({
      id: new FormControl(id, Validators.required),
      weight: new FormControl(weight, Validators.required)});
    (this.editProductForm.get('essentials') as FormArray).push(control);
    console.log(this.editProductForm);
  }

  deleteEssential(index: number) {
    (this.editProductForm.get('essentials') as FormArray).removeAt(index);
  }
}
