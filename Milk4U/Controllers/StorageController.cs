using Milk4U.DAL.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Milk4U.Services.Abstractions;

namespace Milk4U.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StorageController : ControllerBase
    {
        private readonly IStorageService _storageService;

        public StorageController(IStorageService storageService)
        {
            _storageService = storageService;
        }

        [HttpGet]
        public ActionResult<List<StorageViewModel>> GetAll()
        {
            var result = _storageService.GetAll();
            return result;
        }

        [HttpGet("{id}")]
        public ActionResult<StorageViewModel> GetById(int id)
        {
            var result = _storageService.GetById(id);
            return result;
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _storageService.Delete(id);
            return NoContent();
        }

        [HttpDelete("")]
        public IActionResult DeleteExpired()
        {
            _storageService.DeleteExpiredEssentials();
            return NoContent();
        }
    }
}
