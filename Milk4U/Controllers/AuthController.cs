using Microsoft.AspNetCore.Mvc;
using Milk4U.DAL.Enums;
using Milk4U.DAL.Models;
using Milk4U.DAL.ViewModel;
using Milk4U.Services.Abstractions;

namespace Milk4U.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;

        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }

        [HttpPost]
        public ActionResult<User> AddUser(User user)
        {
            var result = _authService.AddUser(user);
            return result;
        }

        [HttpPost("login")]
        public ActionResult<User> LogIn(LogInViewModel logInInfo)
        {
            var result = _authService.LogIn(logInInfo.UserName, logInInfo.Password);
            return result;
        }
    }
}
