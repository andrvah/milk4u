using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Milk4U.DAL.Models;
using Milk4U.DAL.ViewModel;
using Milk4U.Services.Abstractions;

namespace Milk4U.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RevenueController : ControllerBase
    {
        private readonly IRevenueService _revenueService;

        public RevenueController(IRevenueService revenueService)
        {
            _revenueService = revenueService;
        }

        [HttpGet]
        public ActionResult<List<RevenueViewModel>> GetAll()
        {
            var result = _revenueService.GetAll();
            return result;
        }

        [HttpGet("{id}")]
        public ActionResult<RevenueViewModel> GetById(int id)
        {
            var result = _revenueService.GetById(id);
            return result;
        }

        [HttpPost]
        public ActionResult<Revenue> Post(DateTime startDate, DateTime endDate)
        {
            try
            {
                return _revenueService.Calculate(startDate, endDate);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _revenueService.Delete(id);
            return NoContent();
        }
    }
}
