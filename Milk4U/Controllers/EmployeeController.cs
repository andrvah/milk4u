using System;
using Milk4U.DAL.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Milk4U.Services.Abstractions;

namespace Milk4U.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeService _employeeService;

        public EmployeeController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        [HttpGet]
        public ActionResult<List<EmployeeViewModel>> GetAll()
        {
            var result = _employeeService.GetAll();
            return result;
        }

        [HttpGet("{id}")]
        public ActionResult<EmployeeViewModel> GetById(int id)
        {
            var result = _employeeService.GetById(id);
            return result;
        }

        [HttpPost]
        public ActionResult<EmployeeViewModel> Post(EmployeeViewModel employee)
        {
            try
            {
                return _employeeService.Insert(employee);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public ActionResult<EmployeeViewModel> Put(EmployeeViewModel employee)
        {
            var result = _employeeService.Update(employee);
            return result;
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _employeeService.Delete(id);
            return NoContent();
        }
    }
}
