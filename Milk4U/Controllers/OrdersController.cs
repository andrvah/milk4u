using System;
using Milk4U.DAL.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Milk4U.Services.Abstractions;

namespace Milk4U.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderService _ordersService;

        public OrdersController(IOrderService orderService)
        {
            _ordersService = orderService;
        }

        [HttpGet]
        public ActionResult<List<OrdersViewModel>> GetAll()
        {
            var result = _ordersService.GetAll();
            return result;
        }

        [HttpGet("{id}")]
        public ActionResult<OrdersViewModel> GetById(int id)
        {
            var result = _ordersService.GetById(id);
            return result;
        }

        [HttpGet("salers")]
        public ActionResult<List<SalersViewModel>> GetAllSalers()
        {
            return _ordersService.GetAllSalers();
        }       
        
        [HttpGet("customers")]
        public ActionResult<List<CustomersViewModel>> GetAllCustomers()
        {
            return _ordersService.GetAllCustomers();
        }

        [HttpPost]
        public ActionResult<OrdersViewModel> Post(OrderInsertViewModel order)
        {
            try
            {
                return _ordersService.Insert(order);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("return")]
        public ActionResult<ReturnsViewModel> Reutrn(ReturnsViewModel orderReturn)
        {
            try
            {
                return _ordersService.Return(orderReturn);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public ActionResult<OrdersViewModel> Put(OrdersViewModel order)
        {
            var result = _ordersService.Update(order);
            return result;
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _ordersService.Delete(id);
            return NoContent();
        }
    }
}
