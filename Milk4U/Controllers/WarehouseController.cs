using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Milk4U.DAL.ViewModel;
using Milk4U.Services.Abstractions;

namespace Milk4U.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WarehouseController : ControllerBase
    {
        private readonly IWarehouseService _warehouseService;

        public WarehouseController(IWarehouseService warehouseService)
        {
            _warehouseService = warehouseService;
        }

        [HttpGet]
        public ActionResult<List<WarehouseViewModel>> GetAll()
        {
            var result = _warehouseService.GetAll();
            return result;
        }

        [HttpGet("{id}")]
        public ActionResult<WarehouseViewModel> GetById(int id)
        {
            var result = _warehouseService.GetById(id);
            return result;
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _warehouseService.Delete(id);
            return NoContent();
        }

        [HttpDelete("")]
        public IActionResult DeleteExpired()
        {
            _warehouseService.DeleteExpiredProducts();
            return NoContent();
        }
    }
}
