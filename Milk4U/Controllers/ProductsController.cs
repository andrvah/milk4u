using System;
using Milk4U.DAL.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Milk4U.Services.Abstractions;

namespace Milk4U.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProductService _productService;

        public ProductsController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpGet]
        public ActionResult<List<ProductsViewModel>> GetAll()
        {
            var result = _productService.GetAll();
            return result;
        }

        [HttpGet("packages")]
        public ActionResult<List<PackageViewModel>> GetAllPackages()
        {
            var result = _productService.GetAllPackages();
            return result;
        }

        [HttpGet("nutritionals")]
        public ActionResult<List<NutritionalViewModel>> GetAllNutritionals()
        {
            var result = _productService.GetAllNutritionals();
            return result;
        }

        [HttpGet("expirations")]
        public ActionResult<List<ExpirationViewModel>> GetAllExpirations()
        {
            var result = _productService.GetAllExpirations();
            return result;
        }

        [HttpGet("{id}")]
        public ActionResult<ProductsViewModel> GetById(int id)
        {
            var result = _productService.GetById(id);
            return result;
        }

        [HttpPost]
        public ActionResult<ProductsViewModel> Post(ProductsViewModel product)
        {
            try
            {
                return _productService.Insert(product);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("produce")]
        public ActionResult<WarehouseViewModel> Produce(int productId, int amount)
        {
            try
            {
                return _productService.ProduceProduct(productId, amount);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (KeyNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpPut]
        public ActionResult<ProductsViewModel> Put(ProductsViewModel product)
        {
            var result = _productService.Update(product);
            return result;
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _productService.Delete(id);
            return NoContent();
        }
    }
}
