using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Milk4U.DAL.ViewModel;
using Milk4U.Services.Abstractions;

namespace Milk4U.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EssentialsController : ControllerBase
    {
        private readonly IEssentialService _essentialsService;

        public EssentialsController(IEssentialService essentialService)
        {
            _essentialsService = essentialService;
        }

        [HttpGet]
        public ActionResult<List<EssentialsViewModel>> GetAll()
        {
            var result = _essentialsService.GetAll();
            return result;
        }

        [HttpGet("providers")]
        public ActionResult<List<ProvidersViewModel>> GetAllProviders()
        {
            var result = _essentialsService.GetAllProviders();
            return result;
        }

        [HttpGet("{id}")]
        public ActionResult<EssentialsViewModel> GetById(int id)
        {
            var result = _essentialsService.GetById(id);
            return result;
        }

        [HttpPost]
        public ActionResult<EssentialsViewModel> Post(EssentialsViewModel employee)
        {
            try
            {
                return _essentialsService.Insert(employee);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("order")]
        public ActionResult<List<StorageViewModel>> Order(EssentialOrdersViewModel essential)
        {
            var result = _essentialsService.Order(essential);
            return result;
        }

        [HttpPut]
        public ActionResult<EssentialsViewModel> Put(EssentialsViewModel employee)
        {
            var result = _essentialsService.Update(employee);
            return result;
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _essentialsService.Delete(id);
            return NoContent();
        }
    }
}
