using System;
using Milk4U.DAL.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Milk4U.Services.Abstractions;

namespace Milk4U.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShiftsController : ControllerBase
    {
        private readonly IShiftService _shiftService;

        public ShiftsController(IShiftService shiftService)
        {
            _shiftService = shiftService;
        }

        [HttpGet]
        public ActionResult<List<ShiftsViewModel>> GetAll()
        {
            var result = _shiftService.GetAll();
            return result;
        }

        [HttpGet("{id}")]
        public ActionResult<ShiftsViewModel> GetById(int id)
        {
            var result = _shiftService.GetById(id);
            return result;
        }

        [HttpPost]
        public ActionResult<ShiftsViewModel> Post(ShiftsViewModel employee)
        {
            try
            {
                return _shiftService.Insert(employee);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("schedule")]
        public ActionResult<ShiftsScheduleViewModel> Post(ShiftsScheduleViewModel schedule)
        {
            try
            {
                return _shiftService.ScheduleInsert(schedule);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public ActionResult<ShiftsViewModel> Put(ShiftsViewModel employee)
        {
            var result = _shiftService.Update(employee);
            return result;
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _shiftService.Delete(id);
            return NoContent();
        }
    }
}
