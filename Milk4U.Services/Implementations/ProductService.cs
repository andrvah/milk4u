using AutoMapper;
using Milk4U.DAL.Abstaractions;
using Milk4U.DAL.Models;
using Milk4U.DAL.ViewModel;
using Milk4U.Services.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Milk4U.Services.Implementations
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWork _unitOfWork;
        private IMapper _mapper;

        public ProductService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Delete(int id)
        {
            _unitOfWork.ProductsRepository.Delete(id);
            _unitOfWork.Save();
        }

        public List<ProductsViewModel> GetAll()
        {
            var products = _unitOfWork.ProductsRepository.GetAll().ToList();
            var productsDTOs = _mapper.Map<List<Products>, List<ProductsViewModel>>(products);
            return productsDTOs;
        }

        public ProductsViewModel GetById(int id)
        {
            var product = _unitOfWork.ProductsRepository.GetById(id);
            if (product == null)
                throw new Exception("Not found");

            var productDTO = _mapper.Map<Products, ProductsViewModel>(product);
            return productDTO;
        }

        public ProductsViewModel Insert(ProductsViewModel productsDTO)
        {
            var product = _mapper.Map<ProductsViewModel, Products>(productsDTO);
            ConstructChildEntities(product, productsDTO);
            productsDTO.Id = _unitOfWork.ProductsRepository.Add(product).Id;
            _unitOfWork.Save();
            return productsDTO;
        }

        public WarehouseViewModel ProduceProduct(int productId, int amount)
        {
            int shiftId = 1;
            try
            {
                shiftId = _unitOfWork.ShiftsScheduleRepository
                .GetAll()
                .First(shft => shft.Date.Date == DateTime.Now.Date)
                .ShiftId;
            } catch (Exception ex)
            {
                throw new KeyNotFoundException("Сьогодні ніхто не працює");
            }

            var productEssentials = _unitOfWork.ProductsRepository.GetById(productId).ProductsEssentals;
            foreach (var essential in productEssentials)
            {
                var totalWeight = essential.Weight * amount;
                var essentialOnStorage = _unitOfWork.StorageRepository
                    .GetAll()
                    .Where(ess => ess.EssentialId == essential.EssentialId);
                
                foreach (var ess in essentialOnStorage)
                {
                    totalWeight -= ess.Weight;
                    if (totalWeight <= 0)
                    {
                        ess.Weight = -totalWeight;
                        _unitOfWork.StorageRepository.Edit(ess);
                        break;
                    }
                    else
                        ess.Weight = 0;

                    _unitOfWork.StorageRepository.Delete(ess.Id);
                }

                if (totalWeight > 0)
                    throw new Exception("Недостатньо складників для виготовлення");
            }

            

            Random random = new Random();
            Warehouse warehouseItem = new Warehouse()
            {
                ProductId = productId,
                Date = DateTime.Now.AddDays(random.Next(10, 40)),
                Amount = amount,
                ShiftId = shiftId
            };

            _unitOfWork.WarehouseRepository.Add(warehouseItem);
            _unitOfWork.Save();

            var warehouseDTO = _mapper.Map<Warehouse, WarehouseViewModel>(warehouseItem);

            return warehouseDTO;
        }

        public ProductsViewModel Update(ProductsViewModel productDTO)
        {
            var product = _mapper.Map<ProductsViewModel, Products>(productDTO);
            product = _unitOfWork.ProductsRepository.Edit(product);

            ConstructChildEntities(product, productDTO);
            if (product.ExpirationId != 0)
                _unitOfWork.ExpirationRepository.Edit(product.Expiration);
            else
                _unitOfWork.ExpirationRepository.Add(product.Expiration);
            if (product.NutritionalId != 0)
                _unitOfWork.NutritionalRepository.Edit(product.Nutritional);
            else
                _unitOfWork.NutritionalRepository.Add(product.Nutritional);
            if (product.PackageId != 0)
                _unitOfWork.PackageRepository.Edit(product.Package);
            else
                _unitOfWork.PackageRepository.Add(product.Package);

            var oldEssentials = _unitOfWork.ProductEssentialRepository.GetAll().Where(essential => essential.ProductId == product.Id).ToList();
            foreach (var essentals in oldEssentials)
                _unitOfWork.ProductEssentialRepository.Delete(essentals.Id);

            foreach (var essential in productDTO.ProductsEssentals)
            {
                ProductsEssentals essentals = new ProductsEssentals()
                {
                    EssentialId = essential.Essential.Id,
                    Essential = _unitOfWork.EssentialsRepository.GetById(essential.Essential.Id),
                    ProductId = product.Id,
                    Weight = essential.Weight
                };
                _unitOfWork.ProductEssentialRepository.Add(essentals);
            }

            _unitOfWork.Save();
            productDTO = _mapper.Map<Products, ProductsViewModel>(product);
            return productDTO;
        }

        public List<ExpirationViewModel> GetAllExpirations()
        {
            var expirations = _unitOfWork.ExpirationRepository.GetAll().ToList();
            var expirationsVM = _mapper.Map<List<Expiration>, List<ExpirationViewModel>>(expirations);
            return expirationsVM;
        }

        public List<NutritionalViewModel> GetAllNutritionals()
        {
            var nutritionals = _unitOfWork.NutritionalRepository.GetAll().ToList();
            var nutritionalsVM = _mapper.Map<List<Nutritional>, List<NutritionalViewModel>>(nutritionals);
            return nutritionalsVM;
        }

        public List<PackageViewModel> GetAllPackages()
        {
            var packages = _unitOfWork.PackageRepository.GetAll().ToList();
            var packagesVM = _mapper.Map<List<Package>, List<PackageViewModel>>(packages);
            return packagesVM;
        }

        private void ConstructChildEntities(Products product, ProductsViewModel productsDTO)
        {
            if (product.ExpirationId != 0)
                product.Expiration = _unitOfWork.ExpirationRepository.GetById((int)product.ExpirationId);
            else
                product.Expiration = new Expiration()
                {
                    DaysAmount = productsDTO.Expiration.DaysAmount,
                    Temperature = productsDTO.Expiration.Temperature
                };
            if (product.NutritionalId != 0)
                product.Nutritional = _unitOfWork.NutritionalRepository.GetById((int)product.NutritionalId);
            else
                product.Nutritional = new Nutritional()
                {
                    Calories = productsDTO.Nutritional.Calories,
                    Proteins = productsDTO.Nutritional.Proteins,
                    Fats = productsDTO.Nutritional.Fats,
                    Carbohydrates = productsDTO.Nutritional.Carbohydrates
                };
            if (product.PackageId != 0)
                product.Package = _unitOfWork.PackageRepository.GetById((int)product.PackageId);
            else
                product.Package = new Package()
                {
                    Type = productsDTO.Package.Type,
                    Volume = productsDTO.Package.Volume
                };

            for (int i = 0; i < product.ProductsEssentals.Count; i++)
                product.ProductsEssentals.ElementAt(i).Essential = _unitOfWork.EssentialsRepository.GetById(product.ProductsEssentals.ElementAt(i).EssentialId);
        }
    }
}
