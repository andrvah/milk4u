using AutoMapper;
using Milk4U.DAL.Abstaractions;
using Milk4U.DAL.Models;
using Milk4U.DAL.ViewModel;
using Milk4U.Services.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Milk4U.Services.Implementations
{
    public class RevenueService : IRevenueService
    {
        private readonly IUnitOfWork _unitOfWork;
        private IMapper _mapper;

        public RevenueService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public Revenue Calculate(DateTime startDate, DateTime endDate)
        {
            decimal incomes, expenses;

            incomes = 0;
            var orders = _unitOfWork.OrdersRepository
                .GetAll()
                .Where(order => order.Date >= startDate && order.Date <= endDate)
                .Where(order => order.Returns.Count == 0);
            foreach (var order in orders)
                incomes += (decimal) order.Price;

            expenses = 0;
            var essentialOrders = _unitOfWork.EssentialOrdersRepository
                .GetAll()
                .Where(order => order.Date >= startDate && order.Date <= endDate);
            foreach (var order in essentialOrders)
                expenses += (decimal) order.Price;

            double periodInDays = (endDate - startDate).TotalDays;
            var employees = _unitOfWork.EmployeeRepository.GetAll();
            foreach (var employee in employees)
                expenses += (decimal) (employee.Salary * (decimal) periodInDays);

            Revenue revenue = new Revenue()
            {
                Expenses = expenses,
                Incomes = incomes,
                StartDate = startDate,
                EndDate = endDate
            };

            _unitOfWork.RevenueRepository.Add(revenue);
            _unitOfWork.Save();

            return revenue;
        }

        public void Delete(int id)
        {
            _unitOfWork.RevenueRepository.Delete(id);
            _unitOfWork.Save();
        }

        public List<RevenueViewModel> GetAll()
        {
            var revenue = _unitOfWork.RevenueRepository.GetAll().ToList();
            var revenueDTOs = _mapper.Map<List<Revenue>, List<RevenueViewModel>>(revenue);
            return revenueDTOs;
        }

        public RevenueViewModel GetById(int id)
        {
            var revenue = _unitOfWork.RevenueRepository.GetById(id);
            if (revenue == null)
                throw new Exception("Not found");

            var revenueDTO = _mapper.Map<Revenue, RevenueViewModel>(revenue);
            return revenueDTO;
        }
    }
}
