using AutoMapper;
using Milk4U.DAL.Abstaractions;
using Milk4U.DAL.Models;
using Milk4U.DAL.ViewModel;
using Milk4U.Services.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Milk4U.Services.Implementations
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork _unitOfWork;
        private IMapper _mapper;

        public OrderService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Delete(int id)
        {
            _unitOfWork.OrdersRepository.Delete(id);
            _unitOfWork.Save();
        }

        public List<OrdersViewModel> GetAll()
        {
            var orders = _unitOfWork.OrdersRepository.GetAll().ToList();
            var ordersDTOs = _mapper.Map<List<Orders>, List<OrdersViewModel>>(orders);
            return ordersDTOs;
        }

        public List<CustomersViewModel> GetAllCustomers()
        {
            var customers = _unitOfWork.CustomerRepository.GetAll().ToList();
            var customersDTOs = _mapper.Map<List<CustomersViewModel>>(customers);
            return customersDTOs;
        }

        public List<SalersViewModel> GetAllSalers()
        {
            var salers = _unitOfWork.SalersRepository.GetAll().ToList();
            var salersDTOs = _mapper.Map<List<SalersViewModel>>(salers);
            return salersDTOs;
        }

        public OrdersViewModel GetById(int id)
        {
            var order = _unitOfWork.OrdersRepository.GetById(id);
            if (order == null)
                throw new Exception("Not found");

            var orderDTO = _mapper.Map<Orders, OrdersViewModel>(order);
            return orderDTO;
        }

        public OrdersViewModel Insert(OrderInsertViewModel orderDTO)
        {
            var order = _mapper.Map<OrderInsertViewModel, Orders>(orderDTO);
            if (orderDTO.CustomerId != 0)
                order.Customer = _unitOfWork.CustomerRepository.GetById((int)order.CustomerId);
            if (orderDTO.SalerId != 0)
                order.Saler = _unitOfWork.SalersRepository.GetById((int)order.SalerId);
            order.Date = DateTime.Now;
            order.Price = 0;
            foreach (var product in orderDTO.OrdersProducts)
            {
                var amount = product.Amount;
                var warehouseItems = _unitOfWork.WarehouseRepository.GetAll().Where(p => p.ProductId == product.ProductId);

                foreach (var warItem in warehouseItems)
                {
                    amount -= warItem.Amount;
                    if (amount <= 0)
                    {
                        warItem.Amount = -amount;
                        _unitOfWork.WarehouseRepository.Edit(warItem);
                        break;
                    }
                    else
                        warItem.Amount = 0;

                    _unitOfWork.WarehouseRepository.Delete(warItem.Id);
                }

                if (amount > 0)
                    throw new Exception("Неможливо сформувати замовлення через брак продуктів");

                var productPrice = _unitOfWork
                    .ProductsRepository
                    .GetById(product.ProductId)
                    .Price;

                order.Price += product.Amount * productPrice;
            }

            if (order.Customer.Discount != null && order.Customer.Discount != 0)
                order.Price *= order.Customer.Discount;

            _unitOfWork.OrdersRepository.Add(order);
            _unitOfWork.Save();
            order = _unitOfWork.OrdersRepository.GetById(order.Id);
            var orderToReturn = _mapper.Map<Orders, OrdersViewModel>(order);
            return orderToReturn;
        }

        public ReturnsViewModel Return(ReturnsViewModel orderReturnDTO)
        {
            var orderReturn = _mapper.Map<ReturnsViewModel, Returns>(orderReturnDTO);
            orderReturn.Order = _unitOfWork.OrdersRepository.GetById((int)orderReturn.OrderId);
            orderReturn.Date = DateTime.Now;
            _unitOfWork.ReturnsRepository.Add(orderReturn);
            _unitOfWork.Save();
            orderReturnDTO.Id = orderReturn.Id;
            return orderReturnDTO;
        }

        public OrdersViewModel Update(OrdersViewModel orderDTO)
        {
            var order = _mapper.Map<OrdersViewModel, Orders>(orderDTO);
            order = _unitOfWork.OrdersRepository.Edit(order);
            _unitOfWork.Save();
            orderDTO = _mapper.Map<Orders, OrdersViewModel>(order);
            return orderDTO;
        }
    }
}
