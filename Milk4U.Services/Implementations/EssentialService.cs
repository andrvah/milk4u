using AutoMapper;
using Milk4U.DAL.Abstaractions;
using Milk4U.DAL.Models;
using Milk4U.DAL.ViewModel;
using Milk4U.Services.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Milk4U.Services.Implementations
{
    public class EssentialService : IEssentialService
    {
        private readonly IUnitOfWork _unitOfWork;
        private IMapper _mapper;

        public EssentialService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Delete(int id)
        {
            _unitOfWork.EssentialsRepository.Delete(id);
            _unitOfWork.Save();
        }

        public List<EssentialsViewModel> GetAll()
        {
            var essentials = _unitOfWork.EssentialsRepository.GetAll().ToList();
            var essentialsDTOs = _mapper.Map<List<Essentials>, List<EssentialsViewModel>>(essentials);
            return essentialsDTOs;
        }

        public EssentialsViewModel GetById(int id)
        {
            var essential = _unitOfWork.EssentialsRepository.GetById(id);
            if (essential == null)
                throw new Exception("Not found");

            var essentialsDTO = _mapper.Map<Essentials, EssentialsViewModel>(essential);
            return essentialsDTO;
        }

        public EssentialsViewModel Insert(EssentialsViewModel essentialDTO)
        {
            var essential = _mapper.Map<EssentialsViewModel, Essentials>(essentialDTO);
            essentialDTO.Id = _unitOfWork.EssentialsRepository.Add(essential).Id;
            _unitOfWork.Save();
            return essentialDTO;
        }

        public List<ProvidersViewModel> GetAllProviders()
        {
            var providers = _unitOfWork.ProvidersRepository.GetAll();
            var providersDTO = _mapper.Map<List<ProvidersViewModel>>(providers);
            return providersDTO;
        }

        public List<StorageViewModel> Order(EssentialOrdersViewModel order)
        {
            var essOrder = _mapper.Map<EssentialOrdersViewModel, EssentialOrders>(order);
            if (essOrder.ProviderId != 0)
                essOrder.Provider = _unitOfWork.ProvidersRepository.GetById((int)essOrder.ProviderId);
            foreach (var ess in essOrder.EssentialsEssentialOrders)
            {
                ess.Essential = _unitOfWork.EssentialsRepository.GetById(ess.EssentialId);
            }
            essOrder.Date = DateTime.Now;

            _unitOfWork.EssentialOrdersRepository.Add(essOrder);

            Random random = new Random();
            List<Storage> storage = new List<Storage>();
            foreach (var ess in essOrder.EssentialsEssentialOrders)
            {
                Storage storageItem = new Storage()
                {
                    Date = DateTime.Now.AddDays(random.Next(2, 10)),
                    EssentialId = ess.EssentialId,
                    Essential = ess.Essential,
                    Weight = ess.Weight
                };

                storage.Add(storageItem);
                _unitOfWork.StorageRepository.Add(storageItem);
            }

            _unitOfWork.Save();

            var storageDTO = _mapper.Map<List<Storage>, List<StorageViewModel>>(storage);
            return storageDTO;
        }

        public EssentialsViewModel Update(EssentialsViewModel essentialDTO)
        {
            var essential = _mapper.Map<EssentialsViewModel, Essentials>(essentialDTO);
            essential = _unitOfWork.EssentialsRepository.Edit(essential);
            _unitOfWork.Save();
            essentialDTO = _mapper.Map<Essentials, EssentialsViewModel>(essential);
            return essentialDTO;
        }
    }
}
