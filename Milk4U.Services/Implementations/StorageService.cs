using AutoMapper;
using Milk4U.DAL.Abstaractions;
using Milk4U.DAL.Models;
using Milk4U.DAL.ViewModel;
using Milk4U.Services.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Milk4U.Services.Implementations
{
    public class StorageService : IStorageService
    {
        private readonly IUnitOfWork _unitOfWork;
        private IMapper _mapper;

        public StorageService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Delete(int id)
        {
            _unitOfWork.StorageRepository.Delete(id);
            _unitOfWork.Save();
        }

        public void DeleteExpiredEssentials()
        {
            var expiredEssentials = _unitOfWork.StorageRepository
                .GetAll()
                .Where(ess => ess.Date <= DateTime.Now)
                .ToList();

            foreach (var ess in expiredEssentials)
                _unitOfWork.StorageRepository.Delete(ess.Id);

            _unitOfWork.Save();
        }

        public List<StorageViewModel> GetAll()
        {
            var storage = _unitOfWork.StorageRepository.GetAll().ToList();
            var storageDTOs = _mapper.Map<List<Storage>, List<StorageViewModel>>(storage);
            return storageDTOs;
        }

        public StorageViewModel GetById(int id)
        {
            var storage = _unitOfWork.StorageRepository.GetById(id);
            if (storage == null)
                throw new Exception("Not found");

            var storageDTO = _mapper.Map<Storage, StorageViewModel>(storage);
            return storageDTO;
        }
    }
}
