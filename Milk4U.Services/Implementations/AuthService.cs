using Milk4U.DAL.Abstaractions;
using Milk4U.DAL.Enums;
using Milk4U.DAL.Models;
using Milk4U.Services.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Milk4U.Services.Implementations
{
    public class AuthService : IAuthService
    {
        private readonly IUnitOfWork _unitOfWork;

        public AuthService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public User AddUser(User user)
        {
            var insertedUser = _unitOfWork.AuthRepository.Add(user);
            _unitOfWork.Save();
            return insertedUser;
        }

        public User LogIn(string userName, string password)
        {
            return _unitOfWork.AuthRepository.GetAll()
                .First(user => user.UserName == userName && user.Password == password);
        }
    }
}
