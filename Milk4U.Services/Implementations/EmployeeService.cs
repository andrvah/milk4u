using AutoMapper;
using Milk4U.DAL.Abstaractions;
using Milk4U.DAL.Models;
using Milk4U.DAL.ViewModel;
using Milk4U.Services.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Milk4U.Services.Implementations
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IUnitOfWork _unitOfWork;
        private IMapper _mapper;

        public EmployeeService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Delete(int id)
        {
            var shift = _unitOfWork.ShiftsRepository.GetAll().FirstOrDefault(s => s.Employee.Any(e => e.Id == id));
            if (shift != null)
            {
                shift.WorkersNumber--;
                _unitOfWork.ShiftsRepository.Edit(shift);
            }
            _unitOfWork.EmployeeRepository.Delete(id);
            _unitOfWork.Save();
        }

        public List<EmployeeViewModel> GetAll()
        {
            var employees = _unitOfWork.EmployeeRepository.GetAll().ToList();
            var employeesDTOs = _mapper.Map<List<Employee>, List<EmployeeViewModel>>(employees);
            return employeesDTOs;
        }

        public EmployeeViewModel GetById(int id)
        {
            var employee = _unitOfWork.EmployeeRepository.GetById(id);
            if (employee == null)
                throw new Exception("Not found");

            var employeeDTO = _mapper.Map<Employee, EmployeeViewModel>(employee);
            return employeeDTO;
        }

        public EmployeeViewModel Insert(EmployeeViewModel employeeDTO)
        {
            var employee = _mapper.Map<EmployeeViewModel, Employee>(employeeDTO);
            employeeDTO.Id = _unitOfWork.EmployeeRepository.Add(employee).Id;
            _unitOfWork.Save();

            return employeeDTO;
        }

        public EmployeeViewModel Update(EmployeeViewModel employeeDTO)
        {
            var employee = _mapper.Map<EmployeeViewModel, Employee>(employeeDTO);
            employee = _unitOfWork.EmployeeRepository.Edit(employee);
            _unitOfWork.ContactRepository.Edit(employee.Contact);
            if (employee.ShiftId != null)
                employee.Shift = _unitOfWork.ShiftsRepository.GetById((int)employee.ShiftId);
            _unitOfWork.Save();
            employeeDTO = _mapper.Map<Employee, EmployeeViewModel>(employee);
            return employeeDTO;
        }
    }
}
