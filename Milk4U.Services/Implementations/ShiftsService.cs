using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Milk4U.DAL.Abstaractions;
using Milk4U.DAL.Models;
using Milk4U.DAL.ViewModel;
using Milk4U.Services.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Milk4U.Services.Implementations
{
    public class ShiftsService : IShiftService
    {
        private readonly IUnitOfWork _unitOfWork;
        private IMapper _mapper;

        public ShiftsService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Delete(int id)
        {
            var schedule = _unitOfWork.ShiftsScheduleRepository.GetAll().Where(s => s.ShiftId == id).ToList();
            foreach (var shifts in schedule)
                _unitOfWork.ShiftsScheduleRepository.Delete(shifts.Id);
            _unitOfWork.ShiftsRepository.Delete(id);
            _unitOfWork.Save();
        }

        public List<ShiftsViewModel> GetAll()
        {
            var shifts = _unitOfWork.ShiftsRepository.GetAll().ToList();

            foreach (var shift in shifts)
            {
                shift.ShiftsSchedule = shift.ShiftsSchedule.Where(s => s.Date.Date >= DateTime.Now.Date)
                                                           .OrderBy(s => s.Date).ToList();
            }

            var shiftsDTOs = _mapper.Map<List<Shifts>, List<ShiftsViewModel>>(shifts);
            return shiftsDTOs;
        }

        public ShiftsViewModel GetById(int id)
        {
            var shift = _unitOfWork.ShiftsRepository.GetById(id);
            if (shift == null)
                throw new Exception("Not found");

            var employeeDTO = _mapper.Map<Shifts, ShiftsViewModel>(shift);
            return employeeDTO;
        }

        public ShiftsViewModel Insert(ShiftsViewModel shiftDTO)
        {
            var shift = _mapper.Map<ShiftsViewModel, Shifts>(shiftDTO);
            List<Employee> scheduleWorkers = new List<Employee>();
            for (int i = 0; i < shiftDTO.Employee.Count; i++)
            {
                if (shiftDTO.Employee[i].Id != 0)
                {
                    var worker = _unitOfWork.EmployeeRepository.GetById(shiftDTO.Employee[i].Id);
                    if (worker.Shift != null)
                    {
                        worker.Shift.WorkersNumber--;
                        _unitOfWork.ShiftsRepository.Edit(worker.Shift);
                    }
                    scheduleWorkers.Add(worker);
                }
                else
                    scheduleWorkers.Add(shift.Employee.ElementAt(i));
            }
            foreach (var schedule in shift.ShiftsSchedule)
                schedule.Date = schedule.Date.AddHours(5);

            shift.Employee = scheduleWorkers;
            shift.WorkersNumber = scheduleWorkers.Count;

            shiftDTO.Id = _unitOfWork.ShiftsRepository.Add(shift).Id;
            _unitOfWork.Save();
            return shiftDTO;
        }

        public ShiftsScheduleViewModel ScheduleInsert(ShiftsScheduleViewModel scheduleDTO)
        {
            var schedule = _mapper.Map<ShiftsScheduleViewModel, ShiftsSchedule>(scheduleDTO);
            _unitOfWork.ShiftsScheduleRepository.Add(schedule);
            _unitOfWork.Save();
            scheduleDTO.Id = schedule.Id;
            return scheduleDTO;
        }

        public ShiftsViewModel Update(ShiftsViewModel shiftDTO)
        {
            var shift = _mapper.Map<ShiftsViewModel, Shifts>(shiftDTO);
            shift.WorkersNumber = shift.Employee.Count;
            shift = _unitOfWork.ShiftsRepository.Edit(shift);
            foreach (var emp in shiftDTO.Employee)
            {
                var employee = _unitOfWork.EmployeeRepository.GetById(emp.Id);
                if (employee.Shift != null && employee?.ShiftId != shift.Id)
                {
                    employee.Shift.WorkersNumber--;
                    _unitOfWork.ShiftsRepository.Edit(employee.Shift);
                }
                employee.ShiftId = shift.Id;
                _unitOfWork.EmployeeRepository.Edit(employee);
            }

            var oldDates = _unitOfWork.ShiftsScheduleRepository.GetAll().Where(date => date.ShiftId == shift.Id).ToList();
            foreach (var date in oldDates)
                _unitOfWork.ShiftsScheduleRepository.Delete(date.Id);

            foreach (var date in shiftDTO.ShiftsSchedule)
            {
                ShiftsSchedule schedule = new ShiftsSchedule()
                {
                    Date = date.Date.AddHours(26),
                    ShiftId = shift.Id
                };
                _unitOfWork.ShiftsScheduleRepository.Add(schedule);
            }

            _unitOfWork.Save();
            shiftDTO = _mapper.Map<Shifts, ShiftsViewModel>(shift);
            return shiftDTO;
        }
    }
}
