using AutoMapper;
using Milk4U.DAL.Abstaractions;
using Milk4U.DAL.Models;
using Milk4U.DAL.ViewModel;
using Milk4U.Services.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Milk4U.Services.Implementations
{
    public class WarehouseService : IWarehouseService
    {
        private readonly IUnitOfWork _unitOfWork;
        private IMapper _mapper;

        public WarehouseService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Delete(int id)
        {
            _unitOfWork.WarehouseRepository.Delete(id);
            _unitOfWork.Save();
        }

        public void DeleteExpiredProducts()
        {
            var expiredProducts = _unitOfWork.WarehouseRepository
                .GetAll()
                .Where(war => war.Date <= DateTime.Now)
                .ToList();

            foreach (var prod in expiredProducts)
                _unitOfWork.WarehouseRepository.Delete(prod.Id);

            _unitOfWork.Save();
        }

        public List<WarehouseViewModel> GetAll()
        {
            var warehouse = _unitOfWork.WarehouseRepository.GetAll().ToList();
            var warehouseDTOs = _mapper.Map<List<Warehouse>, List<WarehouseViewModel>>(warehouse);
            return warehouseDTOs;
        }

        public WarehouseViewModel GetById(int id)
        {
            var warehouse = _unitOfWork.WarehouseRepository.GetById(id);
            if (warehouse == null)
                throw new Exception("Not found");

            var warehouseDTO = _mapper.Map<Warehouse, WarehouseViewModel>(warehouse);
            return warehouseDTO;
        }
    }
}
