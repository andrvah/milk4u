using Milk4U.DAL.ViewModel;
using System.Collections.Generic;

namespace Milk4U.Services.Abstractions
{
    public interface IOrderService
    {
        List<OrdersViewModel> GetAll();
        List<CustomersViewModel> GetAllCustomers();
        List<SalersViewModel> GetAllSalers();
        OrdersViewModel GetById(int id);
        OrdersViewModel Insert(OrderInsertViewModel product);
        OrdersViewModel Update(OrdersViewModel product);
        ReturnsViewModel Return(ReturnsViewModel orderReturn);
        void Delete(int id);
    }
}
