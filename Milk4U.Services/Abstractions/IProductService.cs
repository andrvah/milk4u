using Milk4U.DAL.ViewModel;
using System.Collections.Generic;

namespace Milk4U.Services.Abstractions
{
    public interface IProductService
    {
        List<ProductsViewModel> GetAll();

        List<ExpirationViewModel> GetAllExpirations();

        List<NutritionalViewModel> GetAllNutritionals();

        List<PackageViewModel> GetAllPackages();

        ProductsViewModel GetById(int id);

        WarehouseViewModel ProduceProduct(int productId, int amount);

        ProductsViewModel Insert(ProductsViewModel product);

        ProductsViewModel Update(ProductsViewModel product);

        void Delete(int id);
    }
}
