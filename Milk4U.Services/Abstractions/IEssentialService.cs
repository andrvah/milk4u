using Milk4U.DAL.ViewModel;
using System.Collections.Generic;

namespace Milk4U.Services.Abstractions
{
    public interface IEssentialService
    {
        List<EssentialsViewModel> GetAll();

        EssentialsViewModel GetById(int id);

        EssentialsViewModel Insert(EssentialsViewModel product);

        EssentialsViewModel Update(EssentialsViewModel product);

        List<StorageViewModel> Order(EssentialOrdersViewModel order);

        List<ProvidersViewModel> GetAllProviders();

        void Delete(int id);
    }
}
