using Milk4U.DAL.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Milk4U.Services.Abstractions
{
    public interface IShiftService
    {
        List<ShiftsViewModel> GetAll();

        ShiftsViewModel GetById(int id);

        ShiftsViewModel Insert(ShiftsViewModel product);

        public ShiftsScheduleViewModel ScheduleInsert(ShiftsScheduleViewModel scheduleDTO);

        ShiftsViewModel Update(ShiftsViewModel product);

        void Delete(int id);
    }
}
