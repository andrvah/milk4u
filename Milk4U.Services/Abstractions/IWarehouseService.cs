using Milk4U.DAL.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Milk4U.Services.Abstractions
{
    public interface IWarehouseService
    {
        List<WarehouseViewModel> GetAll();

        WarehouseViewModel GetById(int id);

        void Delete(int id);

        void DeleteExpiredProducts();
    }
}
