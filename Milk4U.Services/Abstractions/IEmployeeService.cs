using Milk4U.DAL.ViewModel;
using System.Collections.Generic;

namespace Milk4U.Services.Abstractions
{
    public interface IEmployeeService
    {
        List<EmployeeViewModel> GetAll();

        EmployeeViewModel GetById(int id);

        EmployeeViewModel Insert(EmployeeViewModel product);

        EmployeeViewModel Update(EmployeeViewModel product);

        void Delete(int id);
    }
}
