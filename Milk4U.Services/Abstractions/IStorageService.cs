using Milk4U.DAL.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Milk4U.Services.Abstractions
{
    public interface IStorageService
    {
        List<StorageViewModel> GetAll();

        StorageViewModel GetById(int id);

        void Delete(int id);

        void DeleteExpiredEssentials();
    }
}
