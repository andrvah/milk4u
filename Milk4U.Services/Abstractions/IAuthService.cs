using Milk4U.DAL.Enums;
using Milk4U.DAL.Models;

namespace Milk4U.Services.Abstractions
{
    public interface IAuthService
    {
        User LogIn(string userName, string password);
        User AddUser(User user);
    }
}
