using Milk4U.DAL.Models;
using Milk4U.DAL.ViewModel;
using System;
using System.Collections.Generic;

namespace Milk4U.Services.Abstractions
{
    public interface IRevenueService
    {
        List<RevenueViewModel> GetAll();

        RevenueViewModel GetById(int id);

        Revenue Calculate(DateTime startDate, DateTime endDate);

        void Delete(int id);
    }
}
