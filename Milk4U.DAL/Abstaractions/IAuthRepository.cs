using Milk4U.DAL.Enums;
using Milk4U.DAL.Models;

namespace Milk4U.DAL.Abstaractions
{
    public interface IAuthRepository : IRepository<User>
    {
        UserRole GetRole(string userName, string password);
    }
}
