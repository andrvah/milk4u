namespace Milk4U.DAL.Abstaractions
{
    public interface IUnitOfWork
    {
        ICustomerRepository CustomerRepository { get; }
        IEmployeeRepository EmployeeRepository { get; }
        IEssentialOrdersRepository EssentialOrdersRepository { get; }
        IEssentialsEssentialOrdersRepository EssentialsEssentialOrdersRepository { get; }
        IEssentialsRepository EssentialsRepository { get; }
        IOrdersRepository OrdersRepository { get; }
        IProductsRepository ProductsRepository { get; }
        IProvidersRepository ProvidersRepository { get; }
        IReturnsRepository ReturnsRepository { get; }
        IRevenueRepository RevenueRepository { get; }
        ISalersRepository SalersRepository { get; }
        IShiftsRepository ShiftsRepository { get; }
        IShiftsScheduleRepository ShiftsScheduleRepository { get; }
        IStorageRepository StorageRepository { get; }
        IWarehouseRepository WarehouseRepository { get; }
        IAuthRepository AuthRepository { get; }
        IPackageRepository PackageRepository { get; }
        INutritionalRepository NutritionalRepository { get; }
        IExpirationRepository ExpirationRepository { get; }
        IProductEssentialRepository ProductEssentialRepository { get; }
        IContactRepository ContactRepository { get; }
        void Save();
    }
}
