using System;
using System.Linq;
using System.Linq.Expressions;

namespace Milk4U.DAL
{
    public interface IRepository<TEntity> where TEntity : class
    {
        TEntity GetById(int id);

        IQueryable<TEntity> GetAll();

        IQueryable<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate);

        TEntity Add(TEntity entity);

        void Delete(int id);

        TEntity Edit(TEntity entity);
    }
}
