using Milk4U.DAL.Models;

namespace Milk4U.DAL.Abstaractions
{
    public interface IEssentialsRepository : IRepository<Essentials>
    {
    }
}
