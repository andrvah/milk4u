using Milk4U.DAL.Abstaractions;
using System.Collections.Generic;

namespace Milk4U.DAL.Models
{
    public class Account : IEntity
    {
        public Account()
        {
            Customers = new HashSet<Customers>();
            Providers = new HashSet<Providers>();
        }

        public int Id { get; set; }
        public string Recipient { get; set; }
        public decimal? Mfo { get; set; }
        public decimal? Okpo { get; set; }
        public string Currency { get; set; }

        public virtual ICollection<Customers> Customers { get; set; }
        public virtual ICollection<Providers> Providers { get; set; }
    }
}
