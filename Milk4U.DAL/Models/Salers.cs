using Milk4U.DAL.Abstaractions;
using System.Collections.Generic;

namespace Milk4U.DAL.Models
{
    public class Salers : IEntity
    {
        public Salers()
        {
            Orders = new HashSet<Orders>();
        }

        public int Id { get; set; }
        public int? ContactId { get; set; }

        public virtual Contacts Contact { get; set; }
        public virtual ICollection<Orders> Orders { get; set; }
    }
}
