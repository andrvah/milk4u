using Milk4U.DAL.Abstaractions;
using Milk4U.DAL.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Milk4U.DAL.Models
{
    public class User : IEntity
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public UserRole Role { get; set; }
    }
}
