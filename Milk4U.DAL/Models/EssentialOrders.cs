using Milk4U.DAL.Abstaractions;
using System;
using System.Collections.Generic;

namespace Milk4U.DAL.Models
{
    public class EssentialOrders : IEntity
    {
        public EssentialOrders()
        {
            EssentialsEssentialOrders = new HashSet<EssentialsEssentialOrders>();
        }

        public int Id { get; set; }
        public decimal? Price { get; set; }
        public int? ProviderId { get; set; }
        public DateTime? Date { get; set; }

        public virtual Providers Provider { get; set; }
        public virtual ICollection<EssentialsEssentialOrders> EssentialsEssentialOrders { get; set; }
    }
}
