using Milk4U.DAL.Abstaractions;
using System.Collections.Generic;

namespace Milk4U.DAL.Models
{
    public class Expiration : IEntity
    {
        public Expiration()
        {
            Products = new HashSet<Products>();
        }

        public int Id { get; set; }
        public decimal? Temperature { get; set; }
        public int? DaysAmount { get; set; }

        public virtual ICollection<Products> Products { get; set; }
    }
}
