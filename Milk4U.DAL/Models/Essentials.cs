using Milk4U.DAL.Abstaractions;
using System.Collections.Generic;

namespace Milk4U.DAL.Models
{
    public class Essentials : IEntity
    {
        public Essentials()
        {
            EssentialsEssentialOrders = new HashSet<EssentialsEssentialOrders>();
            ProductsEssentals = new HashSet<ProductsEssentals>();
            Storage = new HashSet<Storage>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<EssentialsEssentialOrders> EssentialsEssentialOrders { get; set; }
        public virtual ICollection<ProductsEssentals> ProductsEssentals { get; set; }
        public virtual ICollection<Storage> Storage { get; set; }
    }
}
