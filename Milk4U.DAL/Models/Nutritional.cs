using Milk4U.DAL.Abstaractions;
using System.Collections.Generic;

namespace Milk4U.DAL.Models
{
    public class Nutritional : IEntity
    {
        public Nutritional()
        {
            Products = new HashSet<Products>();
        }

        public int Id { get; set; }
        public decimal? Fats { get; set; }
        public decimal? Proteins { get; set; }
        public decimal? Carbohydrates { get; set; }
        public decimal? Calories { get; set; }

        public virtual ICollection<Products> Products { get; set; }
    }
}
