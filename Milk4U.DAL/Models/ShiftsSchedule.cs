using Milk4U.DAL.Abstaractions;
using System;

namespace Milk4U.DAL.Models
{
    public class ShiftsSchedule : IEntity
    {
        public int Id { get; set; }
        public int ShiftId { get; set; }
        public DateTime Date { get; set; }

        public virtual Shifts Shift { get; set; }
    }
}
