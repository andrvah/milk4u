using Milk4U.DAL.Abstaractions;
using System.Collections.Generic;

namespace Milk4U.DAL.Models
{
    public class Shifts : IEntity
    {
        public Shifts()
        {
            Employee = new HashSet<Employee>();
            //Products = new HashSet<Products>();
            ShiftsSchedule = new HashSet<ShiftsSchedule>();
        }

        public int Id { get; set; }
        public int? WorkersNumber { get; set; }

        public virtual ICollection<Employee> Employee { get; set; }
        //public virtual ICollection<Products> Products { get; set; }
        public virtual ICollection<ShiftsSchedule> ShiftsSchedule { get; set; }
    }
}
