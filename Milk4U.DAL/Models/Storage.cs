using Milk4U.DAL.Abstaractions;
using System;

namespace Milk4U.DAL.Models
{
    public class Storage : IEntity
    {
        public int Id { get; set; }
        public int EssentialId { get; set; }
        public decimal? Weight { get; set; }
        public DateTime Date { get; set; }

        public virtual Essentials Essential { get; set; }
    }
}
