using Milk4U.DAL.Abstaractions;
using System.Collections.Generic;

namespace Milk4U.DAL.Models
{
    public class Contacts : IEntity
    {
        public Contacts()
        {
            Customers = new HashSet<Customers>();
            Employee = new HashSet<Employee>();
            Providers = new HashSet<Providers>();
            Salers = new HashSet<Salers>();
        }

        public int Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string PhoneNumber { get; set; }
        public string EMail { get; set; }

        public virtual ICollection<Customers> Customers { get; set; }
        public virtual ICollection<Employee> Employee { get; set; }
        public virtual ICollection<Providers> Providers { get; set; }
        public virtual ICollection<Salers> Salers { get; set; }
    }
}
