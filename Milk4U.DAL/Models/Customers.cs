using Milk4U.DAL.Abstaractions;
using System.Collections.Generic;

namespace Milk4U.DAL.Models
{
    public class Customers : IEntity
    {
        public Customers()
        {
            Orders = new HashSet<Orders>();
        }

        public int Id { get; set; }
        public int? AccountId { get; set; }
        public decimal? Discount { get; set; }
        public int? ContactId { get; set; }

        public virtual Account Account { get; set; }
        public virtual Contacts Contact { get; set; }
        public virtual ICollection<Orders> Orders { get; set; }
    }
}
