using Milk4U.DAL.Abstaractions;
using System.Collections.Generic;

namespace Milk4U.DAL.Models
{
    public class Products : IEntity
    {
        public Products()
        {
            OrdersProducts = new HashSet<OrdersProducts>();
            ProductsEssentals = new HashSet<ProductsEssentals>();
            Warehouse = new HashSet<Warehouse>();
        }

        public int Id { get; set; }
        public decimal? Price { get; set; }
        public string Name { get; set; }
        public int? NutritionalId { get; set; }
        public int? PackageId { get; set; }
        public int? ExpirationId { get; set; }

        public virtual Expiration Expiration { get; set; }
        public virtual Nutritional Nutritional { get; set; }
        public virtual Package Package { get; set; }
        public virtual ICollection<OrdersProducts> OrdersProducts { get; set; }
        public virtual ICollection<ProductsEssentals> ProductsEssentals { get; set; }
        public virtual ICollection<Warehouse> Warehouse { get; set; }
    }
}
