using Milk4U.DAL.Abstaractions;
using System;

namespace Milk4U.DAL.Models
{
    public class Warehouse : IEntity
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int ShiftId { get; set; }
        public decimal? Amount { get; set; }
        public DateTime? Date { get; set; }

        public virtual Products Product { get; set; }
    }
}
