using Milk4U.DAL.Abstaractions;
using System;

namespace Milk4U.DAL.Models
{
    public class Returns : IEntity
    {
        public int Id { get; set; }
        public string Reason { get; set; }
        public DateTime? Date { get; set; }
        public int? OrderId { get; set; }

        public virtual Orders Order { get; set; }
    }
}
