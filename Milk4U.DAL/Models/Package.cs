using Milk4U.DAL.Abstaractions;
using System.Collections.Generic;

namespace Milk4U.DAL.Models
{
    public class Package : IEntity
    {
        public Package()
        {
            Products = new HashSet<Products>();
        }

        public int Id { get; set; }
        public string Type { get; set; }
        public decimal? Volume { get; set; }

        public virtual ICollection<Products> Products { get; set; }
    }
}
