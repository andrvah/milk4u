using Milk4U.DAL.Abstaractions;

namespace Milk4U.DAL.Models
{
    public class OrdersProducts : IEntity
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public decimal? Amount { get; set; }

        public virtual Orders Order { get; set; }
        public virtual Products Product { get; set; }
    }
}
