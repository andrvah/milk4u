using Milk4U.DAL.Abstaractions;

namespace Milk4U.DAL.Models
{
    public class EssentialsEssentialOrders : IEntity
    {
        public int Id { get; set; }
        public int EssentialId { get; set; }
        public int OrderId { get; set; }
        public decimal? Weight { get; set; }

        public virtual Essentials Essential { get; set; }
        public virtual EssentialOrders Order { get; set; }
    }
}
