using Milk4U.DAL.Abstaractions;

namespace Milk4U.DAL.Models
{
    public class Employee : IEntity
    {
        public int Id { get; set; }
        public string Position { get; set; }
        public decimal? Salary { get; set; }
        public int? ContactId { get; set; }
        public int? ShiftId { get; set; }

        public virtual Contacts Contact { get; set; }
        public virtual Shifts Shift { get; set; }
    }
}
