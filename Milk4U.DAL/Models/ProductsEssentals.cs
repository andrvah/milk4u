using Milk4U.DAL.Abstaractions;

namespace Milk4U.DAL.Models
{
    public class ProductsEssentals : IEntity
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int EssentialId { get; set; }
        public decimal? Weight { get; set; }

        public virtual Essentials Essential { get; set; }
        public virtual Products Product { get; set; }
    }
}
