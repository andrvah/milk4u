using Milk4U.DAL.Abstaractions;
using System;
using System.Collections.Generic;

namespace Milk4U.DAL.Models
{
    public class Orders : IEntity
    {
        public Orders()
        {
            OrdersProducts = new HashSet<OrdersProducts>();
            Returns = new HashSet<Returns>();
        }

        public int Id { get; set; }
        public int? CustomerId { get; set; }
        public int? SalerId { get; set; }
        public decimal? Price { get; set; }
        public DateTime? Date { get; set; }

        public virtual Customers Customer { get; set; }
        public virtual Salers Saler { get; set; }
        public virtual ICollection<OrdersProducts> OrdersProducts { get; set; }
        public virtual ICollection<Returns> Returns { get; set; }
    }
}
