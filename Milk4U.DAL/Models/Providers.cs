using Milk4U.DAL.Abstaractions;
using System.Collections.Generic;

namespace Milk4U.DAL.Models
{
    public class Providers : IEntity
    {
        public Providers()
        {
            EssentialOrders = new HashSet<EssentialOrders>();
        }

        public int Id { get; set; }
        public decimal? Rating { get; set; }
        public int? ContactId { get; set; }
        public int? AccountId { get; set; }

        public virtual Account Account { get; set; }
        public virtual Contacts Contact { get; set; }
        public virtual ICollection<EssentialOrders> EssentialOrders { get; set; }
    }
}
