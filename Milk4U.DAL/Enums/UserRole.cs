using System.ComponentModel;

namespace Milk4U.DAL.Enums
{
    public enum UserRole : byte
    {
        [Description("SuperAdmin")]
        SuperAdmin = 0,

        [Description("Sales manager")]
        SalesManager,

        [Description("Production manager")]
        ProductionManager
    }
}
