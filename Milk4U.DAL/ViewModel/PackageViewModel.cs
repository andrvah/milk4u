using System.Collections.Generic;

namespace Milk4U.DAL.ViewModel
{
    public class PackageViewModel
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public decimal? Volume { get; set; }
    }
}
