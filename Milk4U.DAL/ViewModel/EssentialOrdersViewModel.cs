using System;
using System.Collections.Generic;

namespace Milk4U.DAL.ViewModel
{
    public class EssentialOrdersViewModel
    {
        public int Id { get; set; }
        public decimal? Price { get; set; }
        public DateTime? Date { get; set; }

        public ProvidersViewModel Provider { get; set; }
        public IList<EssentialsEssentialOrdersViewModel> EssentialsEssentialOrders { get; set; }
    }
}
