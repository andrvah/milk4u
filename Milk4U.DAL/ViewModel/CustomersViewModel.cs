namespace Milk4U.DAL.ViewModel
{
    public class CustomersViewModel
    {
        public int Id { get; set; }
        public decimal? Discount { get; set; }
        public AccountViewModel Account { get; set; }
        public ContactsViewModel Contact { get; set; }
    }
}
