using System;

namespace Milk4U.DAL.ViewModel
{
    public class StorageViewModel 
    {
        public int Id { get; set; }
        public decimal? Weight { get; set; }
        public DateTime Date { get; set; }

        public EssentialsViewModel Essential { get; set; }
    }
}
