using System;

namespace Milk4U.DAL.ViewModel
{
    public class RevenueViewModel
    {
        public int Id { get; set; }
        public decimal? Incomes { get; set; }
        public decimal? Expenses { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
