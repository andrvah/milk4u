using System;
using System.Collections.Generic;

namespace Milk4U.DAL.ViewModel 
{
    public class ExpirationViewModel
    {
        public int Id { get; set; }
        public decimal? Temperature { get; set; }
        public int? DaysAmount { get; set; }
    }
}
