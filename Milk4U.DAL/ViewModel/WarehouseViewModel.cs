using System;

namespace Milk4U.DAL.ViewModel
{
    public class WarehouseViewModel
    {
        public int Id { get; set; }
        public decimal? Amount { get; set; }
        public DateTime? Date { get; set; }
        public int? ShiftId { get; set; }

        public ProductsViewModel Product { get; set; }
    }
}
