using System;
using System.Collections.Generic;

namespace Milk4U.DAL.ViewModel
{
    public class OrdersViewModel
    {
        public int Id { get; set; }
        public decimal? Price { get; set; }
        public DateTime? Date { get; set; }

        public CustomersViewModel Customer { get; set; }
        public SalersViewModel Saler { get; set; }
        public IList<OrdersProductsViewModel> OrdersProducts { get; set; }
        public IList<ReturnsViewModel> Returns { get; set; }
    }
}
