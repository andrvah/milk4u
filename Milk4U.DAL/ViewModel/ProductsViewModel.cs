using System.Collections.Generic;

namespace Milk4U.DAL.ViewModel
{
    public class ProductsViewModel
    {
        public int Id { get; set; }
        public decimal? Price { get; set; }
        public string Name { get; set; }

        public ExpirationViewModel Expiration { get; set; }
        public NutritionalViewModel Nutritional { get; set; }
        public PackageViewModel Package { get; set; }
        public ShiftsViewModel Shift { get; set; }
        public IList<OrdersProductsViewModel> OrdersProducts { get; set; }
        public IList<ProductsEssentalsViewModel> ProductsEssentals { get; set; }
        //public IList<WarehouseViewModel> Warehouse { get; set; }
    }
}
