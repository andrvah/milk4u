using System;
using System.Collections.Generic;

namespace Milk4U.DAL.ViewModel
{
    public class OrderInsertViewModel
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int SalerId { get; set; }
        public IList<OrdersProductsViewModel> OrdersProducts { get; set; }
        public CustomersViewModel Customer { get; set; }
        public SalersViewModel Saler { get; set; }
    }
}
