namespace Milk4U.DAL.ViewModel
{
    public class AccountViewModel
    {
        public int Id { get; set; }
        public string Recipient { get; set; }
        public decimal? Mfo { get; set; }
        public decimal? Okpo { get; set; }
        public string Currency { get; set; }
    }
}
