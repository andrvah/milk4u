namespace Milk4U.DAL.ViewModel
{
    public class OrdersProductsViewModel
    {
        public int Id { get; set; }
        public decimal? Amount { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }

        //public OrdersViewModel Order { get; set; }
    }
}
