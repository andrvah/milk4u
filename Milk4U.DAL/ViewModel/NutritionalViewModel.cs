using System.Collections.Generic;

namespace Milk4U.DAL.ViewModel
{
    public class NutritionalViewModel
    {
        public int Id { get; set; }
        public decimal? Fats { get; set; }
        public decimal? Proteins { get; set; }
        public decimal? Carbohydrates { get; set; }
        public decimal? Calories { get; set; }
    }
}
