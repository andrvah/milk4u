using System.Collections.Generic;

namespace Milk4U.DAL.ViewModel
{
    public class ProvidersViewModel
    {
        public int Id { get; set; }
        public decimal? Rating { get; set; }

        public AccountViewModel Account { get; set; }
        public ContactsViewModel Contact { get; set; }
    }
}
