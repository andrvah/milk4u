using System;

namespace Milk4U.DAL.ViewModel
{
    public class ReturnsViewModel
    {
        public int Id { get; set; }
        public string Reason { get; set; }
        public int? OrderId { get; set; }
        public DateTime? Date { get; set; }

       // public OrdersViewModel Order { get; set; }
    }
}
