namespace Milk4U.DAL.ViewModel
{
    public class EmployeeViewModel
    {
        public int Id { get; set; }
        public string Position { get; set; }
        public decimal? Salary { get; set; }
        public int? ShiftId { get; set; }

        public virtual ContactsViewModel Contact { get; set; }
       // public virtual ShiftsViewModel Shift { get; set; }
    }
}
