using System;

namespace Milk4U.DAL.ViewModel
{
    public class ShiftsScheduleViewModel
    {
        public int Id { get; set; }

        public int ShiftId { get; set; }
        public DateTime Date { get; set; }

       // public ShiftsViewModel Shift { get; set; }
    }
}
