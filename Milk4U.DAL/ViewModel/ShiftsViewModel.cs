using System.Collections.Generic;

namespace Milk4U.DAL.ViewModel
{
    public class ShiftsViewModel
    {
        public int Id { get; set; }
        public int? WorkersNumber { get; set; }
        public IList<EmployeeViewModel> Employee { get; set; }
        public IList<ShiftsScheduleViewModel> ShiftsSchedule { get; set; }
    }
}
