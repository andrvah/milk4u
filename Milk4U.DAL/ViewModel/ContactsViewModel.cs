namespace Milk4U.DAL.ViewModel
{
    public class ContactsViewModel
    {
        public int Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string PhoneNumber { get; set; }
        public string EMail { get; set; }
    }
}
