namespace Milk4U.DAL.ViewModel
{
    public class EssentialsEssentialOrdersViewModel
    {
        public int Id { get; set; }
        public decimal? Weight { get; set; }

        public EssentialsViewModel Essential { get; set; }
        //public EssentialOrdersViewModel Order { get; set; }
    }
}
