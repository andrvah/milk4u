namespace Milk4U.DAL.ViewModel
{
    public class ProductsEssentalsViewModel
    {
        public int Id { get; set; }
        public decimal? Weight { get; set; }

        public EssentialsViewModel Essential { get; set; }
       // public ProductsViewModel Product { get; set; }
    }
}
