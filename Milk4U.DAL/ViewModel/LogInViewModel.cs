using System;
using System.Collections.Generic;
using System.Text;

namespace Milk4U.DAL.ViewModel
{
    public class LogInViewModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
