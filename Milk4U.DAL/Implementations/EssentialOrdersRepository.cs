using System.Linq;
using Milk4U.DAL.Models;
using Milk4U.DAL.Abstaractions;
using Microsoft.EntityFrameworkCore;

namespace Milk4U.DAL.Implementations
{
    public class EssentialOrdersRepository : Repository<EssentialOrders>, IEssentialOrdersRepository
    {
        public EssentialOrdersRepository(Milk4UContext context) : base(context)
        {
        }

        public override EssentialOrders GetById(int id)
        {
            return _context.Set<EssentialOrders>()
                           .Include(e => e.Provider)
                           .FirstOrDefault(e => e.Id == id);
        }

        public override IQueryable<EssentialOrders> GetAll()
        {
            return _context.Set<EssentialOrders>()
                           .Include(e => e.Provider);
        }
    }
}
