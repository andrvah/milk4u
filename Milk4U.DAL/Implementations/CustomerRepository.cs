using Microsoft.EntityFrameworkCore;
using Milk4U.DAL.Abstaractions;
using Milk4U.DAL.Models;
using System.Linq;

namespace Milk4U.DAL.Implementations
{
    public class CustomerRepository : Repository<Customers>, ICustomerRepository
    {
        public CustomerRepository(Milk4UContext context) : base(context)
        {
        }

        public override Customers GetById(int id)
        {
            return _context.Set<Customers>()
                           .Include(e => e.Account)
                           .Include(e => e.Contact)
                           .FirstOrDefault(e => e.Id == id);
        }

        public override IQueryable<Customers> GetAll()
        {
            return _context.Set<Customers>()
                           .Include(e => e.Account)
                           .Include(e => e.Contact);
        }
    }
}
