using Microsoft.EntityFrameworkCore;
using Milk4U.DAL.Abstaractions;
using Milk4U.DAL.Models;
using System.Linq;

namespace Milk4U.DAL.Implementations
{
    public class ShiftsScheduleRepository : Repository<ShiftsSchedule>, IShiftsScheduleRepository
    {
        public ShiftsScheduleRepository(Milk4UContext context) : base(context)
        {
        }

        public override ShiftsSchedule GetById(int id)
        {
            return _context.Set<ShiftsSchedule>()
                           .Include(e => e.Shift)
                           .FirstOrDefault(e => e.Id == id);
        }

        public override IQueryable<ShiftsSchedule> GetAll()
        {
            return _context.Set<ShiftsSchedule>()
                           .Include(e => e.Shift);
        }
    }
}
