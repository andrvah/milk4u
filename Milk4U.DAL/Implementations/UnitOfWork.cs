using Milk4U.DAL.Abstaractions;

namespace Milk4U.DAL.Implementations
{
    public class UnitOfWork : IUnitOfWork
    {
        private Milk4UContext _context;
        private ICustomerRepository _customerRepository;
        private IEmployeeRepository _employeeRepository;
        private IEssentialOrdersRepository _essentialOrdersRepository;
        private IEssentialsEssentialOrdersRepository _essentialsEssentialOrdersRepository;
        private IEssentialsRepository _essentialsRepository;
        private IOrdersRepository _ordersRepository;
        private IProductsRepository _productsRepository;
        private IProvidersRepository _providersRepository;
        private IReturnsRepository _returnsRepository;
        private IRevenueRepository _revenueRepository;
        private ISalersRepository _salersRepository;
        private IShiftsRepository _shiftsRepository;
        private IShiftsScheduleRepository _shiftsScheduleRepository;
        private IStorageRepository _storageRepository;
        private IWarehouseRepository _warehouseRepository;
        private IAuthRepository _authRepository;
        private IPackageRepository _packageRepository;
        private INutritionalRepository _nutritionalRepository;
        private IExpirationRepository _expirationRepository;
        private IContactRepository _contactRepository;
        private IProductEssentialRepository _productEssentialRepository;

        public UnitOfWork (Milk4UContext context)
        {
            _context = context;
        }

        public ICustomerRepository CustomerRepository
        {
            get
            {
                return _customerRepository ??= new CustomerRepository(_context);
            }
        }

        public IEmployeeRepository EmployeeRepository
        {
            get
            {
                return _employeeRepository ??= new EmployeeRepository(_context);
            }
        }

        public IEssentialOrdersRepository EssentialOrdersRepository
        {
            get
            {
                return _essentialOrdersRepository ??= new EssentialOrdersRepository(_context);
            }
        }

        public IEssentialsRepository EssentialsRepository
        {
            get
            {
                return _essentialsRepository ??= new EssentialsRepository(_context);
            }
        }

        public IEssentialsEssentialOrdersRepository EssentialsEssentialOrdersRepository
        {
            get
            {
                return _essentialsEssentialOrdersRepository ??= new EssentialsEssentialOrdersRepository(_context);
            }
        }

        public IOrdersRepository OrdersRepository
        {
            get
            {
                return _ordersRepository ??= new OrdersRepository(_context);
            }
        }

        public IProductsRepository ProductsRepository
        {
            get
            {
                return _productsRepository ??= new ProductsRepository(_context);
            }
        }

        public IProvidersRepository ProvidersRepository
        {
            get
            {
                return _providersRepository ??= new ProvidersRepository(_context);
            }
        }

        public IReturnsRepository ReturnsRepository
        {
            get
            {
                return _returnsRepository ??= new ReturnsRepository(_context);
            }
        }

        public IRevenueRepository RevenueRepository
        {
            get
            {
                return _revenueRepository ??= new RevenueRepository(_context);
            }
        }

        public ISalersRepository SalersRepository
        {
            get
            {
                return _salersRepository ??= new SalersRepository(_context);
            }
        }

        public IShiftsRepository ShiftsRepository
        {
            get
            {
                return _shiftsRepository ??= new ShiftsRepository(_context);
            }
        }

        public IShiftsScheduleRepository ShiftsScheduleRepository
        {
            get
            {
                return _shiftsScheduleRepository ??= new ShiftsScheduleRepository(_context);
            }
        }

        public IStorageRepository StorageRepository
        {
            get
            {
                return _storageRepository ??= new StorageRepository(_context);
            }
        }

        public IWarehouseRepository WarehouseRepository
        {
            get
            {
                return _warehouseRepository ??= new WarehouseRepository(_context);
            }
        }

        public IAuthRepository AuthRepository
        {
            get
            {
                return _authRepository ??= new AuthRepository(_context);
            }
        }

        public IPackageRepository PackageRepository
        {
            get
            {
                return _packageRepository ??= new PackageRepository(_context);
            }
        }

        public INutritionalRepository NutritionalRepository
        {
            get
            {
                return _nutritionalRepository ??= new NutritionalRepository(_context);
            }
        }

        public IExpirationRepository ExpirationRepository
        {
            get
            {
                return _expirationRepository ??= new ExpirationRepository(_context);
            }
        }

        public IProductEssentialRepository ProductEssentialRepository
        {
            get
            {
                return _productEssentialRepository ??= new ProductEssentialRepository(_context);
            }
        }

        public IContactRepository ContactRepository
        {
            get
            {
                return _contactRepository ??= new ContactRepository(_context);
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
