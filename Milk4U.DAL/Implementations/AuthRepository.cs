using Milk4U.DAL.Abstaractions;
using Milk4U.DAL.Enums;
using Milk4U.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Milk4U.DAL.Implementations
{
    public class AuthRepository : Repository<User>, IAuthRepository
    {
        public AuthRepository(Milk4UContext context) : base(context)
        {
        }

        public UserRole GetRole(string userName, string password)
        {
            return _context.Set<User>()
                .First(user => user.UserName == userName && user.Password == password)
                .Role;
        }
    }
}
