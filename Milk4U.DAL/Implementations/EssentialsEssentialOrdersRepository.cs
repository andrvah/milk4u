using Milk4U.DAL.Abstaractions;
using Milk4U.DAL.Models;

namespace Milk4U.DAL.Implementations
{
    public class EssentialsEssentialOrdersRepository : Repository<EssentialsEssentialOrders>, IEssentialsEssentialOrdersRepository
    {
        public EssentialsEssentialOrdersRepository(Milk4UContext context) : base(context)
        {
        }
    }
}
