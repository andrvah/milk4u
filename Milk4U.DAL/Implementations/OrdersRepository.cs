using System.Linq;
using Milk4U.DAL.Models;
using Milk4U.DAL.Abstaractions;
using Microsoft.EntityFrameworkCore;

namespace Milk4U.DAL.Implementations
{
    public class OrdersRepository : Repository<Orders>, IOrdersRepository
    {
        public OrdersRepository(Milk4UContext context) : base(context)
        {
        }

        public override Orders GetById(int id)
        {
            return _context.Set<Orders>()
                           .Include(e => e.Customer)
                                .ThenInclude(e => e.Contact)
                           .Include(e => e.Customer)
                                .ThenInclude(e => e.Account)
                           .Include(e => e.Saler)
                                .ThenInclude(e => e.Contact)
                           .Include(e => e.Returns)
                           .Include(e => e.OrdersProducts)
                                .ThenInclude(e => e.Product)
                           .FirstOrDefault(e => e.Id == id);
        }

        public override IQueryable<Orders> GetAll()
        {
            return _context.Set<Orders>()
                           .Include(e => e.Customer)
                                .ThenInclude(e => e.Contact)
                           .Include(e => e.Customer)
                                .ThenInclude(e => e.Account)
                           .Include(e => e.Saler)
                                .ThenInclude(e => e.Contact)
                           .Include(e => e.Returns)
                           .Include(e => e.OrdersProducts)
                                .ThenInclude(e => e.Product);
        }
    }
}
