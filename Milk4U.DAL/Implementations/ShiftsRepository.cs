using Microsoft.EntityFrameworkCore;
using Milk4U.DAL.Abstaractions;
using Milk4U.DAL.Models;
using System.Linq;

namespace Milk4U.DAL.Implementations
{
    public class ShiftsRepository : Repository<Shifts>, IShiftsRepository
    {
        public ShiftsRepository(Milk4UContext context) : base(context)
        {
        }

        public override Shifts GetById(int id)
        {
            return _context.Set<Shifts>()
                           .Include(e => e.Employee)
                                .ThenInclude(e => e.Contact)
                           .Include(e => e.ShiftsSchedule)
                           .FirstOrDefault(e => e.Id == id);
        }

        public override IQueryable<Shifts> GetAll()
        {
            return _context.Set<Shifts>()
                           .Include(e => e.Employee)
                                .ThenInclude(e => e.Contact)
                           .Include(e => e.ShiftsSchedule);
        }
    }
}
