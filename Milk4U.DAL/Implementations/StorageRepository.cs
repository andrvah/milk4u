using Microsoft.EntityFrameworkCore;
using Milk4U.DAL.Abstaractions;
using Milk4U.DAL.Models;
using System.Linq;

namespace Milk4U.DAL.Implementations
{
    public class StorageRepository : Repository<Storage>, IStorageRepository
    {
        public StorageRepository(Milk4UContext context) : base(context)
        {
        }

        public override Storage GetById(int id)
        {
            return _context.Set<Storage>()
                           .Include(e => e.Essential)
                           .FirstOrDefault(e => e.Id == id);
        }

        public override IQueryable<Storage> GetAll()
        {
            return _context.Set<Storage>()
                           .Include(e => e.Essential);
        }
    }
}
