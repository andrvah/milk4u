using Microsoft.EntityFrameworkCore;
using Milk4U.DAL.Abstaractions;
using Milk4U.DAL.Models;
using System.Linq;

namespace Milk4U.DAL.Implementations
{
    public class WarehouseRepository : Repository<Warehouse>, IWarehouseRepository
    {
        public WarehouseRepository(Milk4UContext context) : base(context)
        {
        }

        public override Warehouse GetById(int id)
        {
            return _context.Set<Warehouse>()
                           .Include(e => e.Product)
                                .ThenInclude(e => e.Package)
                           .Include(e => e.Product)
                                .ThenInclude(e => e.Nutritional)
                           .Include(e => e.Product)
                                .ThenInclude(e => e.Expiration)
                           .Include(e => e.Product)
                                .ThenInclude(e => e.ProductsEssentals)
                                    .ThenInclude(e => e.Essential)
                           .Include(e => e.Product)
                           .FirstOrDefault(e => e.Id == id);
        }

        public override IQueryable<Warehouse> GetAll()
        {
            return _context.Set<Warehouse>()
                           .Include(e => e.Product)
                                .ThenInclude(e => e.Package)
                           .Include(e => e.Product)
                                .ThenInclude(e => e.Nutritional)
                           .Include(e => e.Product)
                                .ThenInclude(e => e.Expiration)
                           .Include(e => e.Product)
                                .ThenInclude(e => e.ProductsEssentals)
                                    .ThenInclude(e => e.Essential)
                           .Include(e => e.Product);
        }
    }
}
