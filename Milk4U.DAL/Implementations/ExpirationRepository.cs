using Milk4U.DAL.Abstaractions;
using Milk4U.DAL.Models;

namespace Milk4U.DAL.Implementations
{
    class ExpirationRepository : Repository<Expiration>, IExpirationRepository
    {
        public ExpirationRepository(Milk4UContext context) : base(context)
        {
        }
    }
}
