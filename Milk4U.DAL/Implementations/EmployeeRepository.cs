using System.Linq;
using Milk4U.DAL.Models;
using Milk4U.DAL.Abstaractions;
using Microsoft.EntityFrameworkCore;

namespace Milk4U.DAL.Implementations
{
    public class EmployeeRepository : Repository<Employee>, IEmployeeRepository
    {
        public EmployeeRepository(Milk4UContext context) : base(context)
        {
        }

        public override Employee GetById(int id)
        {
            return _context.Set<Employee>()
                           .Include(e => e.Contact)
                           .Include(e => e.Shift)
                           .FirstOrDefault(e => e.Id == id);
        }

        public override IQueryable<Employee> GetAll()
        {
            return _context.Set<Employee>()
                           .Include(e => e.Contact);
        }
    }
}
