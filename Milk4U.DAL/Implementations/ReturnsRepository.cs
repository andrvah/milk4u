using Microsoft.EntityFrameworkCore;
using Milk4U.DAL.Abstaractions;
using Milk4U.DAL.Models;
using System.Linq;

namespace Milk4U.DAL.Implementations
{
    public class ReturnsRepository : Repository<Returns>, IReturnsRepository
    {
        public ReturnsRepository(Milk4UContext context) : base(context)
        {
        }
    }
}
