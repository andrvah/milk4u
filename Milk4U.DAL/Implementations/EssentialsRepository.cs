using Milk4U.DAL.Models;
using Milk4U.DAL.Abstaractions;

namespace Milk4U.DAL.Implementations
{
    public class EssentialsRepository : Repository<Essentials>, IEssentialsRepository
    {
        public EssentialsRepository(Milk4UContext context) : base(context)
        {
        }
    }
}
