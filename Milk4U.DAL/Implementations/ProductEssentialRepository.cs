using Milk4U.DAL.Abstaractions;
using Milk4U.DAL.Models;

namespace Milk4U.DAL.Implementations
{
    class ProductEssentialRepository : Repository<ProductsEssentals>, IProductEssentialRepository
    {
        public ProductEssentialRepository(Milk4UContext context) : base(context)
        {
        }
    }
}
