using Microsoft.EntityFrameworkCore;
using Milk4U.DAL.Abstaractions;
using Milk4U.DAL.Models;
using System.Linq;

namespace Milk4U.DAL.Implementations
{
    public class ProductsRepository : Repository<Products>, IProductsRepository
    {
        public ProductsRepository(Milk4UContext context) : base(context)
        {
        }

        public override Products GetById(int id)
        {
            return _context.Set<Products>()
                           .Include(e => e.Expiration)
                           .Include(e => e.Nutritional)
                           .Include(e => e.Package)
                           .Include(e => e.ProductsEssentals)
                                .ThenInclude(e => e.Essential)
                           .FirstOrDefault(e => e.Id == id);
        }

        public override IQueryable<Products> GetAll()
        {
            return _context.Set<Products>()
                           .Include(e => e.Expiration)
                           .Include(e => e.Nutritional)
                           .Include(e => e.Package)
                           .Include(e => e.ProductsEssentals)
                                .ThenInclude(e => e.Essential);
        }
    }
}
