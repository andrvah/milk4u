using Milk4U.DAL.Abstaractions;
using Milk4U.DAL.Models;

namespace Milk4U.DAL.Implementations
{
    public class ContactRepository : Repository<Contacts>, IContactRepository
    {
        public ContactRepository(Milk4UContext context) : base(context)
        {
        }
    }
}
