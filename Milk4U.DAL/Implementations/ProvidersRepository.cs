using Microsoft.EntityFrameworkCore;
using Milk4U.DAL.Abstaractions;
using Milk4U.DAL.Models;
using System.Linq;

namespace Milk4U.DAL.Implementations
{
    public class ProvidersRepository : Repository<Providers>, IProvidersRepository
    {
        public ProvidersRepository(Milk4UContext context) : base(context)
        {
        }

        public override Providers GetById(int id)
        {
            return _context.Set<Providers>()
                           .Include(e => e.Account)
                           .Include(e => e.Contact)
                           .FirstOrDefault(e => e.Id == id);
        }

        public override IQueryable<Providers> GetAll()
        {
            return _context.Set<Providers>()
                           .Include(e => e.Account)
                           .Include(e => e.Contact);
        }
    }
}
