using Milk4U.DAL.Abstaractions;
using Milk4U.DAL.Models;

namespace Milk4U.DAL.Implementations
{
    public class PackageRepository : Repository<Package>, IPackageRepository
    {
        public PackageRepository(Milk4UContext context) : base(context)
        {
        }
    }
}
