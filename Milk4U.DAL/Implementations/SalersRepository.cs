using Microsoft.EntityFrameworkCore;
using Milk4U.DAL.Abstaractions;
using Milk4U.DAL.Models;
using System.Linq;

namespace Milk4U.DAL.Implementations
{
    public class SalersRepository : Repository<Salers>, ISalersRepository
    {
        public SalersRepository(Milk4UContext context) : base(context)
        {
        }

        public override Salers GetById(int id)
        {
            return _context.Set<Salers>()
                           .Include(e => e.Contact)
                           .FirstOrDefault(e => e.Id == id);
        }

        public override IQueryable<Salers> GetAll()
        {
            return _context.Set<Salers>()
                           .Include(e => e.Contact);
        }
    }
}
