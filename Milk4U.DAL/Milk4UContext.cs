using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Milk4U.DAL.Models;

namespace Milk4U.DAL
{
    public class Milk4UContext : DbContext
    {
        public Milk4UContext()
        {
        }

        public Milk4UContext(DbContextOptions<Milk4UContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Account> Account { get; set; }
        public virtual DbSet<Contacts> Contacts { get; set; }
        public virtual DbSet<Customers> Customers { get; set; }
        public virtual DbSet<Employee> Employee { get; set; }
        public virtual DbSet<EssentialOrders> EssentialOrders { get; set; }
        public virtual DbSet<Essentials> Essentials { get; set; }
        public virtual DbSet<EssentialsEssentialOrders> EssentialsEssentialOrders { get; set; }
        public virtual DbSet<Expiration> Expiration { get; set; }
        public virtual DbSet<Nutritional> Nutritional { get; set; }
        public virtual DbSet<Orders> Orders { get; set; }
        public virtual DbSet<OrdersProducts> OrdersProducts { get; set; }
        public virtual DbSet<Package> Package { get; set; }
        public virtual DbSet<Products> Products { get; set; }
        public virtual DbSet<ProductsEssentals> ProductsEssentals { get; set; }
        public virtual DbSet<Providers> Providers { get; set; }
        public virtual DbSet<Returns> Returns { get; set; }
        public virtual DbSet<Revenue> Revenue { get; set; }
        public virtual DbSet<Salers> Salers { get; set; }
        public virtual DbSet<Shifts> Shifts { get; set; }
        public virtual DbSet<ShiftsSchedule> ShiftsSchedule { get; set; }
        public virtual DbSet<Storage> Storage { get; set; }
        public virtual DbSet<Warehouse> Warehouse { get; set; }

        public virtual DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("server=localhost; database=Milk4U; Integrated Security=true;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("account_id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Currency)
                    .HasColumnName("currency")
                    .HasMaxLength(3);

                entity.Property(e => e.Mfo)
                    .HasColumnName("MFO")
                    .HasColumnType("decimal(6, 0)");

                entity.Property(e => e.Okpo)
                    .HasColumnName("OKPO")
                    .HasColumnType("decimal(8, 0)");

                entity.Property(e => e.Recipient)
                    .HasColumnName("recipient")
                    .HasMaxLength(80);
            });

            modelBuilder.Entity<Contacts>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("XPKContacts");

                entity.Property(e => e.Id)
                    .HasColumnName("contact_id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.EMail)
                    .HasColumnName("e_mail")
                    .HasMaxLength(80);

                entity.Property(e => e.FirstName)
                    .HasColumnName("first_name")
                    .HasMaxLength(30);

                entity.Property(e => e.LastName)
                    .HasColumnName("last_name")
                    .HasMaxLength(30);

                entity.Property(e => e.PhoneNumber)
                    .HasColumnName("phone_number")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Customers>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("XPKCustomers");

                entity.Property(e => e.Id)
                    .HasColumnName("customer_id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.AccountId).HasColumnName("account_id");

                entity.Property(e => e.ContactId).HasColumnName("contact_id");

                entity.Property(e => e.Discount)
                    .HasColumnName("discount")
                    .HasColumnType("decimal(3, 2)");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.Customers)
                    .HasForeignKey(d => d.AccountId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("R_13");

                entity.HasOne(d => d.Contact)
                    .WithMany(p => p.Customers)
                    .HasForeignKey(d => d.ContactId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("R_8");
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("employee_id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.ContactId).HasColumnName("contact_id");

                entity.Property(e => e.Position)
                    .HasColumnName("position")
                    .HasMaxLength(30);

                entity.Property(e => e.Salary)
                    .HasColumnName("salary")
                    .HasColumnType("decimal(5, 0)");

                entity.Property(e => e.ShiftId).HasColumnName("shift_id");

                entity.HasOne(d => d.Contact)
                    .WithMany(p => p.Employee)
                    .HasForeignKey(d => d.ContactId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("R_130");

                entity.HasOne(d => d.Shift)
                    .WithMany(p => p.Employee)
                    .HasForeignKey(d => d.ShiftId)
                    .HasConstraintName("R_111");
            });

            modelBuilder.Entity<EssentialOrders>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("XPKEssential_Orders");

                entity.ToTable("Essential_Orders");

                entity.Property(e => e.Id)
                    .HasColumnName("order_id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("decimal(15, 2)");

                entity.Property(e => e.ProviderId).HasColumnName("provider_id");

                entity.HasOne(d => d.Provider)
                    .WithMany(p => p.EssentialOrders)
                    .HasForeignKey(d => d.ProviderId)
                    .HasConstraintName("R_129");
            });

            modelBuilder.Entity<Essentials>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("XPKEssentials");

                entity.Property(e => e.Id)
                    .HasColumnName("essential_id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EssentialsEssentialOrders>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("XPKEssentials_Essential_Orders");

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd();

                entity.ToTable("Essentials_Essential_Orders");

                entity.Property(e => e.EssentialId).HasColumnName("essential_id");

                entity.Property(e => e.OrderId).HasColumnName("order_id");

                entity.Property(e => e.Weight)
                    .HasColumnName("weight")
                    .HasColumnType("decimal(15, 6)");

                entity.HasOne(d => d.Essential)
                    .WithMany(p => p.EssentialsEssentialOrders)
                    .HasForeignKey(d => d.EssentialId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_42");

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.EssentialsEssentialOrders)
                    .HasForeignKey(d => d.OrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_54");
            });

            modelBuilder.Entity<Expiration>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("expiration_id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.DaysAmount).HasColumnName("days_amount");

                entity.Property(e => e.Temperature)
                    .HasColumnName("temperature")
                    .HasColumnType("decimal(3, 1)");
            });

            modelBuilder.Entity<Nutritional>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("nutritional_id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Calories)
                    .HasColumnName("calories")
                    .HasColumnType("decimal(6, 0)");

                entity.Property(e => e.Carbohydrates)
                    .HasColumnName("carbohydrates")
                    .HasColumnType("decimal(6, 2)");

                entity.Property(e => e.Fats)
                    .HasColumnName("fats")
                    .HasColumnType("decimal(6, 2)");

                entity.Property(e => e.Proteins)
                    .HasColumnName("proteins")
                    .HasColumnType("decimal(6, 2)");
            });

            modelBuilder.Entity<Orders>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("XPKOrders");

                entity.Property(e => e.Id)
                    .HasColumnName("order_id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.CustomerId).HasColumnName("customer_id");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("decimal(17, 2)");

                entity.Property(e => e.SalerId).HasColumnName("saler_id");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("R_3");

                entity.HasOne(d => d.Saler)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.SalerId)
                    .HasConstraintName("R_6");
            });

            modelBuilder.Entity<OrdersProducts>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("XPKOrders_Products");

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd();

                entity.ToTable("Orders_Products");

                entity.Property(e => e.OrderId).HasColumnName("order_id");

                entity.Property(e => e.ProductId).HasColumnName("product_id");

                entity.Property(e => e.Amount)
                    .HasColumnName("amount")
                    .HasColumnType("decimal(10, 0)");

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.OrdersProducts)
                    .HasForeignKey(d => d.OrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_32");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.OrdersProducts)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_33");
            });

            modelBuilder.Entity<Package>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("package_id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasMaxLength(20);

                entity.Property(e => e.Volume)
                    .HasColumnName("volume")
                    .HasColumnType("decimal(10, 2)");
            });

            modelBuilder.Entity<Products>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("XPKProducts");

                entity.Property(e => e.Id)
                    .HasColumnName("product_id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.ExpirationId).HasColumnName("expiration_id");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(50);

                entity.Property(e => e.NutritionalId).HasColumnName("nutritional_id");

                entity.Property(e => e.PackageId).HasColumnName("package_id");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("decimal(10, 2)");

                entity.HasOne(d => d.Expiration)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.ExpirationId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("R_127");

                entity.HasOne(d => d.Nutritional)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.NutritionalId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("R_17");

                entity.HasOne(d => d.Package)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.PackageId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("R_31");

            });

            modelBuilder.Entity<ProductsEssentals>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("XPKProducts_Essentals");

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd();

                entity.ToTable("Products_Essentals");

                entity.Property(e => e.EssentialId).HasColumnName("essential_id");

                entity.Property(e => e.ProductId).HasColumnName("product_id");

                entity.Property(e => e.Weight)
                    .HasColumnName("weight")
                    .HasColumnType("decimal(15, 5)");

                entity.HasOne(d => d.Essential)
                    .WithMany(p => p.ProductsEssentals)
                    .HasForeignKey(d => d.EssentialId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_112");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductsEssentals)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_113");
            });

            modelBuilder.Entity<Providers>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("XPKProviders");

                entity.Property(e => e.Id)
                    .HasColumnName("provider_id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.AccountId).HasColumnName("account_id");

                entity.Property(e => e.ContactId).HasColumnName("contact_id");

                entity.Property(e => e.Rating)
                    .HasColumnName("rating")
                    .HasColumnType("decimal(2, 0)");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.Providers)
                    .HasForeignKey(d => d.AccountId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("R_12");

                entity.HasOne(d => d.Contact)
                    .WithMany(p => p.Providers)
                    .HasForeignKey(d => d.ContactId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("R_10");
            });

            modelBuilder.Entity<Returns>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("XPKReturns");

                entity.Property(e => e.Id)
                    .HasColumnName("return_id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("datetime");

                entity.Property(e => e.OrderId).HasColumnName("order_id");

                entity.Property(e => e.Reason)
                    .HasColumnName("reason")
                    .HasMaxLength(250);

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.Returns)
                    .HasForeignKey(d => d.OrderId)
                    .HasConstraintName("R_14");
            });

            modelBuilder.Entity<Revenue>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("revenue_id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.EndDate)
                    .HasColumnName("end_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Expenses)
                    .HasColumnName("expenses")
                    .HasColumnType("decimal(16, 2)");

                entity.Property(e => e.Incomes)
                    .HasColumnName("incomes")
                    .HasColumnType("decimal(16, 2)");

                entity.Property(e => e.StartDate)
                    .HasColumnName("start_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Salers>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("XPKSalers");

                entity.Property(e => e.Id)
                    .HasColumnName("saler_id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.ContactId).HasColumnName("contact_id");

                entity.HasOne(d => d.Contact)
                    .WithMany(p => p.Salers)
                    .HasForeignKey(d => d.ContactId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("R_7");
            });

            modelBuilder.Entity<Shifts>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("XPKShifts");

                entity.Property(e => e.Id)
                    .HasColumnName("shift_id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.WorkersNumber).HasColumnName("workers_number");
            });

            modelBuilder.Entity<ShiftsSchedule>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("XPKShifts_Schedule");

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd();

                entity.ToTable("Shifts_Schedule");

                entity.Property(e => e.ShiftId).HasColumnName("shift_id");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.Shift)
                    .WithMany(p => p.ShiftsSchedule)
                    .HasForeignKey(d => d.ShiftId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_25");
            });

            modelBuilder.Entity<Storage>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("datetime");

                entity.Property(e => e.EssentialId).HasColumnName("essential_id");

                entity.Property(e => e.Weight)
                    .HasColumnName("weight")
                    .HasColumnType("decimal(16, 6)");

                entity.HasOne(d => d.Essential)
                    .WithMany(p => p.Storage)
                    .HasForeignKey(d => d.EssentialId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_133");
            });

            modelBuilder.Entity<Warehouse>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Amount)
                    .HasColumnName("amount")
                    .HasColumnType("decimal(15, 0)");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("datetime");

                entity.Property(e => e.ProductId).HasColumnName("product_id");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.Warehouse)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_132");

                
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.UserName)
                    .HasColumnType("nvarchar(128)");

                entity.HasIndex(e => e.UserName)
                    .HasName("IX_Username")
                    .IsUnique();

                entity.Property(e => e.Password)
                    .HasColumnType("nvarchar(128)");
            });
        }
    }
}
