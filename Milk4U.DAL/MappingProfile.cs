using AutoMapper;
using Milk4U.DAL.Models;
using Milk4U.DAL.ViewModel;

namespace Milk4U.DAL
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Account, AccountViewModel>().ReverseMap();
            CreateMap<Contacts, ContactsViewModel>().ReverseMap();
            CreateMap<Customers, CustomersViewModel>().ReverseMap();
            CreateMap<Employee, EmployeeViewModel>().ReverseMap();
            CreateMap<EssentialOrders, EssentialOrdersViewModel>().ReverseMap();
            CreateMap<Essentials, EssentialsViewModel>().ReverseMap();
            CreateMap<EssentialsEssentialOrders, EssentialsEssentialOrdersViewModel>().ReverseMap();
            CreateMap<Expiration, ExpirationViewModel>().ReverseMap();
            CreateMap<Nutritional, NutritionalViewModel>().ReverseMap();
            CreateMap<Orders, OrdersViewModel>().ReverseMap();
            CreateMap<Orders, OrderInsertViewModel>().ReverseMap();
            CreateMap<OrdersProducts, OrdersProductsViewModel>()
                .ForMember(
                    a => a.ProductName, 
                    o => o.MapFrom(p => p.Product.Name)
                );
            CreateMap<OrdersProductsViewModel, OrdersProducts>();
            CreateMap<Package, PackageViewModel>().ReverseMap();
            CreateMap<Products, ProductsViewModel>().ReverseMap();
            CreateMap<ProductsEssentals, ProductsEssentalsViewModel>().ReverseMap();
            CreateMap<Providers, ProvidersViewModel>().ReverseMap();
            CreateMap<Returns, ReturnsViewModel>().ReverseMap();
            CreateMap<Revenue, RevenueViewModel>().ReverseMap();
            CreateMap<Salers, SalersViewModel>().ReverseMap();
            CreateMap<Shifts, ShiftsViewModel>().ReverseMap();
            CreateMap<ShiftsSchedule, ShiftsScheduleViewModel>().ReverseMap();
            CreateMap<Storage, StorageViewModel>().ReverseMap();
            CreateMap<Warehouse, WarehouseViewModel>().ReverseMap();
        }
    }
}
