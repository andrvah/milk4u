﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Milk4U.DAL.Migrations
{
    public partial class shift_product_fix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_Shifts_ShiftsId",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Products_ShiftsId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "ShiftsId",
                table: "Products");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ShiftsId",
                table: "Products",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Products_ShiftsId",
                table: "Products",
                column: "ShiftsId");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Shifts_ShiftsId",
                table: "Products",
                column: "ShiftsId",
                principalTable: "Shifts",
                principalColumn: "shift_id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
