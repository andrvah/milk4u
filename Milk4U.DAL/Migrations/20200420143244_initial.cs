using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Milk4U.DAL.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.CreateTable(
            //    name: "Account",
            //    columns: table => new
            //    {
            //        account_id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        recipient = table.Column<string>(maxLength: 80, nullable: true),
            //        MFO = table.Column<decimal>(type: "decimal(6, 0)", nullable: true),
            //        OKPO = table.Column<decimal>(type: "decimal(8, 0)", nullable: true),
            //        currency = table.Column<string>(maxLength: 3, nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Account", x => x.account_id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Contacts",
            //    columns: table => new
            //    {
            //        contact_id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        last_name = table.Column<string>(maxLength: 30, nullable: true),
            //        first_name = table.Column<string>(maxLength: 30, nullable: true),
            //        phone_number = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
            //        e_mail = table.Column<string>(maxLength: 80, nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("XPKContacts", x => x.contact_id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Essentials",
            //    columns: table => new
            //    {
            //        essential_id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        name = table.Column<string>(maxLength: 50, nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("XPKEssentials", x => x.essential_id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Expiration",
            //    columns: table => new
            //    {
            //        expiration_id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        temperature = table.Column<decimal>(type: "decimal(3, 1)", nullable: true),
            //        days_amount = table.Column<int>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Expiration", x => x.expiration_id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Nutritional",
            //    columns: table => new
            //    {
            //        nutritional_id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        fats = table.Column<decimal>(type: "decimal(5, 2)", nullable: true),
            //        proteins = table.Column<decimal>(type: "decimal(5, 2)", nullable: true),
            //        carbohydrates = table.Column<decimal>(type: "decimal(5, 2)", nullable: true),
            //        calories = table.Column<decimal>(type: "decimal(4, 0)", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Nutritional", x => x.nutritional_id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Package",
            //    columns: table => new
            //    {
            //        package_id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        type = table.Column<string>(maxLength: 20, nullable: true),
            //        volume = table.Column<decimal>(type: "decimal(6, 2)", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Package", x => x.package_id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Revenue",
            //    columns: table => new
            //    {
            //        revenue_id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        incomes = table.Column<decimal>(type: "decimal(12, 2)", nullable: true),
            //        expenses = table.Column<decimal>(type: "decimal(12, 2)", nullable: true),
            //        start_date = table.Column<DateTime>(type: "datetime", nullable: true),
            //        end_date = table.Column<DateTime>(type: "datetime", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Revenue", x => x.revenue_id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Shifts",
            //    columns: table => new
            //    {
            //        shift_id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        workers_number = table.Column<int>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("XPKShifts", x => x.shift_id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Customers",
            //    columns: table => new
            //    {
            //        customer_id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        account_id = table.Column<int>(nullable: true),
            //        discount = table.Column<decimal>(type: "decimal(3, 2)", nullable: true),
            //        contact_id = table.Column<int>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("XPKCustomers", x => x.customer_id);
            //        table.ForeignKey(
            //            name: "R_13",
            //            column: x => x.account_id,
            //            principalTable: "Account",
            //            principalColumn: "account_id",
            //            onDelete: ReferentialAction.Cascade);
            //        table.ForeignKey(
            //            name: "R_8",
            //            column: x => x.contact_id,
            //            principalTable: "Contacts",
            //            principalColumn: "contact_id",
            //            onDelete: ReferentialAction.Cascade);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Providers",
            //    columns: table => new
            //    {
            //        provider_id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        rating = table.Column<decimal>(type: "decimal(2, 0)", nullable: true),
            //        contact_id = table.Column<int>(nullable: true),
            //        account_id = table.Column<int>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("XPKProviders", x => x.provider_id);
            //        table.ForeignKey(
            //            name: "R_12",
            //            column: x => x.account_id,
            //            principalTable: "Account",
            //            principalColumn: "account_id",
            //            onDelete: ReferentialAction.Cascade);
            //        table.ForeignKey(
            //            name: "R_10",
            //            column: x => x.contact_id,
            //            principalTable: "Contacts",
            //            principalColumn: "contact_id",
            //            onDelete: ReferentialAction.Cascade);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Salers",
            //    columns: table => new
            //    {
            //        saler_id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        contact_id = table.Column<int>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("XPKSalers", x => x.saler_id);
            //        table.ForeignKey(
            //            name: "R_7",
            //            column: x => x.contact_id,
            //            principalTable: "Contacts",
            //            principalColumn: "contact_id",
            //            onDelete: ReferentialAction.Cascade);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Storage",
            //    columns: table => new
            //    {
            //        id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        essential_id = table.Column<int>(nullable: false),
            //        weight = table.Column<decimal>(type: "decimal(9, 6)", nullable: true),
            //        date = table.Column<DateTime>(type: "datetime", nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Storage", x => x.id);
            //        table.ForeignKey(
            //            name: "R_133",
            //            column: x => x.essential_id,
            //            principalTable: "Essentials",
            //            principalColumn: "essential_id",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Employee",
            //    columns: table => new
            //    {
            //        employee_id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        position = table.Column<string>(maxLength: 30, nullable: true),
            //        salary = table.Column<decimal>(type: "decimal(5, 0)", nullable: true),
            //        contact_id = table.Column<int>(nullable: true),
            //        shift_id = table.Column<int>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Employee", x => x.employee_id);
            //        table.ForeignKey(
            //            name: "R_130",
            //            column: x => x.contact_id,
            //            principalTable: "Contacts",
            //            principalColumn: "contact_id",
            //            onDelete: ReferentialAction.Cascade);
            //        table.ForeignKey(
            //            name: "R_111",
            //            column: x => x.shift_id,
            //            principalTable: "Shifts",
            //            principalColumn: "shift_id",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Products",
            //    columns: table => new
            //    {
            //        product_id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        price = table.Column<decimal>(type: "decimal(6, 2)", nullable: true),
            //        name = table.Column<string>(maxLength: 50, nullable: true),
            //        nutritional_id = table.Column<int>(nullable: true),
            //        shift_id = table.Column<int>(nullable: true),
            //        package_id = table.Column<int>(nullable: true),
            //        expiration_id = table.Column<int>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("XPKProducts", x => x.product_id);
            //        table.ForeignKey(
            //            name: "R_127",
            //            column: x => x.expiration_id,
            //            principalTable: "Expiration",
            //            principalColumn: "expiration_id",
            //            onDelete: ReferentialAction.Cascade);
            //        table.ForeignKey(
            //            name: "R_17",
            //            column: x => x.nutritional_id,
            //            principalTable: "Nutritional",
            //            principalColumn: "nutritional_id",
            //            onDelete: ReferentialAction.Cascade);
            //        table.ForeignKey(
            //            name: "R_31",
            //            column: x => x.package_id,
            //            principalTable: "Package",
            //            principalColumn: "package_id",
            //            onDelete: ReferentialAction.Cascade);
            //        table.ForeignKey(
            //            name: "R_24",
            //            column: x => x.shift_id,
            //            principalTable: "Shifts",
            //            principalColumn: "shift_id",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Shifts_Schedule",
            //    columns: table => new
            //    {
            //        shift_id = table.Column<int>(nullable: false),
            //        date = table.Column<DateTime>(type: "datetime", nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("XPKShifts_Schedule", x => new { x.shift_id, x.date });
            //        table.ForeignKey(
            //            name: "R_25",
            //            column: x => x.shift_id,
            //            principalTable: "Shifts",
            //            principalColumn: "shift_id",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Essential_Orders",
            //    columns: table => new
            //    {
            //        order_id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        price = table.Column<decimal>(type: "decimal(8, 2)", nullable: true),
            //        provider_id = table.Column<int>(nullable: true),
            //        date = table.Column<DateTime>(type: "datetime", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("XPKEssential_Orders", x => x.order_id);
            //        table.ForeignKey(
            //            name: "R_129",
            //            column: x => x.provider_id,
            //            principalTable: "Providers",
            //            principalColumn: "provider_id",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Orders",
            //    columns: table => new
            //    {
            //        order_id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        customer_id = table.Column<int>(nullable: true),
            //        saler_id = table.Column<int>(nullable: true),
            //        price = table.Column<decimal>(type: "decimal(8, 2)", nullable: true),
            //        date = table.Column<DateTime>(type: "datetime", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("XPKOrders", x => x.order_id);
            //        table.ForeignKey(
            //            name: "R_3",
            //            column: x => x.customer_id,
            //            principalTable: "Customers",
            //            principalColumn: "customer_id",
            //            onDelete: ReferentialAction.Restrict);
            //        table.ForeignKey(
            //            name: "R_6",
            //            column: x => x.saler_id,
            //            principalTable: "Salers",
            //            principalColumn: "saler_id",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Products_Essentals",
            //    columns: table => new
            //    {
            //        product_id = table.Column<int>(nullable: false),
            //        essential_id = table.Column<int>(nullable: false),
            //        weight = table.Column<decimal>(type: "decimal(8, 5)", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("XPKProducts_Essentals", x => new { x.essential_id, x.product_id });
            //        table.ForeignKey(
            //            name: "R_112",
            //            column: x => x.essential_id,
            //            principalTable: "Essentials",
            //            principalColumn: "essential_id",
            //            onDelete: ReferentialAction.Restrict);
            //        table.ForeignKey(
            //            name: "R_113",
            //            column: x => x.product_id,
            //            principalTable: "Products",
            //            principalColumn: "product_id",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Warehouse",
            //    columns: table => new
            //    {
            //        id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        product_id = table.Column<int>(nullable: false),
            //        amount = table.Column<decimal>(type: "decimal(5, 0)", nullable: true),
            //        date = table.Column<DateTime>(type: "datetime", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Warehouse", x => x.id);
            //        table.ForeignKey(
            //            name: "R_132",
            //            column: x => x.product_id,
            //            principalTable: "Products",
            //            principalColumn: "product_id",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Essentials_Essential_Orders",
            //    columns: table => new
            //    {
            //        essential_id = table.Column<int>(nullable: false),
            //        order_id = table.Column<int>(nullable: false),
            //        weight = table.Column<decimal>(type: "decimal(10, 6)", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("XPKEssentials_Essential_Orders", x => new { x.essential_id, x.order_id });
            //        table.ForeignKey(
            //            name: "R_42",
            //            column: x => x.essential_id,
            //            principalTable: "Essentials",
            //            principalColumn: "essential_id",
            //            onDelete: ReferentialAction.Restrict);
            //        table.ForeignKey(
            //            name: "R_54",
            //            column: x => x.order_id,
            //            principalTable: "Essential_Orders",
            //            principalColumn: "order_id",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Orders_Products",
            //    columns: table => new
            //    {
            //        order_id = table.Column<int>(nullable: false),
            //        product_id = table.Column<int>(nullable: false),
            //        amount = table.Column<decimal>(type: "decimal(2, 0)", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("XPKOrders_Products", x => new { x.order_id, x.product_id });
            //        table.ForeignKey(
            //            name: "R_32",
            //            column: x => x.order_id,
            //            principalTable: "Orders",
            //            principalColumn: "order_id",
            //            onDelete: ReferentialAction.Restrict);
            //        table.ForeignKey(
            //            name: "R_33",
            //            column: x => x.product_id,
            //            principalTable: "Products",
            //            principalColumn: "product_id",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Returns",
            //    columns: table => new
            //    {
            //        return_id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        reason = table.Column<string>(maxLength: 250, nullable: true),
            //        date = table.Column<DateTime>(type: "datetime", nullable: true),
            //        order_id = table.Column<int>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("XPKReturns", x => x.return_id);
            //        table.ForeignKey(
            //            name: "R_14",
            //            column: x => x.order_id,
            //            principalTable: "Orders",
            //            principalColumn: "order_id",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateIndex(
            //    name: "IX_Customers_account_id",
            //    table: "Customers",
            //    column: "account_id");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Customers_contact_id",
            //    table: "Customers",
            //    column: "contact_id");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Employee_contact_id",
            //    table: "Employee",
            //    column: "contact_id");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Employee_shift_id",
            //    table: "Employee",
            //    column: "shift_id");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Essential_Orders_provider_id",
            //    table: "Essential_Orders",
            //    column: "provider_id");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Essentials_Essential_Orders_order_id",
            //    table: "Essentials_Essential_Orders",
            //    column: "order_id");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Orders_customer_id",
            //    table: "Orders",
            //    column: "customer_id");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Orders_saler_id",
            //    table: "Orders",
            //    column: "saler_id");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Orders_Products_product_id",
            //    table: "Orders_Products",
            //    column: "product_id");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Products_expiration_id",
            //    table: "Products",
            //    column: "expiration_id");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Products_nutritional_id",
            //    table: "Products",
            //    column: "nutritional_id");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Products_package_id",
            //    table: "Products",
            //    column: "package_id");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Products_shift_id",
            //    table: "Products",
            //    column: "shift_id");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Products_Essentals_product_id",
            //    table: "Products_Essentals",
            //    column: "product_id");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Providers_account_id",
            //    table: "Providers",
            //    column: "account_id");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Providers_contact_id",
            //    table: "Providers",
            //    column: "contact_id");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Returns_order_id",
            //    table: "Returns",
            //    column: "order_id");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Salers_contact_id",
            //    table: "Salers",
            //    column: "contact_id");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Storage_essential_id",
            //    table: "Storage",
            //    column: "essential_id");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Warehouse_product_id",
            //    table: "Warehouse",
            //    column: "product_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropTable(
            //    name: "Employee");

            //migrationBuilder.DropTable(
            //    name: "Essentials_Essential_Orders");

            //migrationBuilder.DropTable(
            //    name: "Orders_Products");

            //migrationBuilder.DropTable(
            //    name: "Products_Essentals");

            //migrationBuilder.DropTable(
            //    name: "Returns");

            //migrationBuilder.DropTable(
            //    name: "Revenue");

            //migrationBuilder.DropTable(
            //    name: "Shifts_Schedule");

            //migrationBuilder.DropTable(
            //    name: "Storage");

            //migrationBuilder.DropTable(
            //    name: "Warehouse");

            //migrationBuilder.DropTable(
            //    name: "Essential_Orders");

            //migrationBuilder.DropTable(
            //    name: "Orders");

            //migrationBuilder.DropTable(
            //    name: "Essentials");

            //migrationBuilder.DropTable(
            //    name: "Products");

            //migrationBuilder.DropTable(
            //    name: "Providers");

            //migrationBuilder.DropTable(
            //    name: "Customers");

            //migrationBuilder.DropTable(
            //    name: "Salers");

            //migrationBuilder.DropTable(
            //    name: "Expiration");

            //migrationBuilder.DropTable(
            //    name: "Nutritional");

            //migrationBuilder.DropTable(
            //    name: "Package");

            //migrationBuilder.DropTable(
            //    name: "Shifts");

            //migrationBuilder.DropTable(
            //    name: "Account");

            //migrationBuilder.DropTable(
            //    name: "Contacts");
        }
    }
}
