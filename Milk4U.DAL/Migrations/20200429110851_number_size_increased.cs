﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Milk4U.DAL.Migrations
{
    public partial class number_size_increased : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "amount",
                table: "Warehouse",
                type: "decimal(15, 0)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(5, 0)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "weight",
                table: "Storage",
                type: "decimal(16, 6)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(9, 6)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "incomes",
                table: "Revenue",
                type: "decimal(16, 2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(12, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "expenses",
                table: "Revenue",
                type: "decimal(16, 2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(12, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "weight",
                table: "Products_Essentals",
                type: "decimal(15, 5)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(8, 5)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "price",
                table: "Products",
                type: "decimal(10, 2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(6, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "volume",
                table: "Package",
                type: "decimal(10, 2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(6, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "amount",
                table: "Orders_Products",
                type: "decimal(10, 0)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(2, 0)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "price",
                table: "Orders",
                type: "decimal(17, 2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(8, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "proteins",
                table: "Nutritional",
                type: "decimal(6, 2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(5, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "fats",
                table: "Nutritional",
                type: "decimal(6, 2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(5, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "carbohydrates",
                table: "Nutritional",
                type: "decimal(6, 2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(5, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "calories",
                table: "Nutritional",
                type: "decimal(6, 0)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(4, 0)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "weight",
                table: "Essentials_Essential_Orders",
                type: "decimal(15, 6)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(10, 6)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "price",
                table: "Essential_Orders",
                type: "decimal(15, 2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(8, 2)",
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "amount",
                table: "Warehouse",
                type: "decimal(5, 0)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(15, 0)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "weight",
                table: "Storage",
                type: "decimal(9, 6)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(16, 6)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "incomes",
                table: "Revenue",
                type: "decimal(12, 2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(16, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "expenses",
                table: "Revenue",
                type: "decimal(12, 2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(16, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "weight",
                table: "Products_Essentals",
                type: "decimal(8, 5)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(15, 5)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "price",
                table: "Products",
                type: "decimal(6, 2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(10, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "volume",
                table: "Package",
                type: "decimal(6, 2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(10, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "amount",
                table: "Orders_Products",
                type: "decimal(2, 0)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(10, 0)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "price",
                table: "Orders",
                type: "decimal(8, 2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(17, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "proteins",
                table: "Nutritional",
                type: "decimal(5, 2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(6, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "fats",
                table: "Nutritional",
                type: "decimal(5, 2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(6, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "carbohydrates",
                table: "Nutritional",
                type: "decimal(5, 2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(6, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "calories",
                table: "Nutritional",
                type: "decimal(4, 0)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(6, 0)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "weight",
                table: "Essentials_Essential_Orders",
                type: "decimal(10, 6)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(15, 6)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "price",
                table: "Essential_Orders",
                type: "decimal(8, 2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(15, 2)",
                oldNullable: true);
        }
    }
}
