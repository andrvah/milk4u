﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Milk4U.DAL.Migrations
{
    public partial class primary_keys_changed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "XPKShifts_Schedule",
                table: "Shifts_Schedule");

            migrationBuilder.DropPrimaryKey(
                name: "XPKProducts_Essentals",
                table: "Products_Essentals");

            migrationBuilder.DropPrimaryKey(
                name: "XPKOrders_Products",
                table: "Orders_Products");

            migrationBuilder.DropPrimaryKey(
                name: "XPKEssentials_Essential_Orders",
                table: "Essentials_Essential_Orders");

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "Shifts_Schedule",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "Products_Essentals",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "Orders_Products",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "Essentials_Essential_Orders",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddPrimaryKey(
                name: "XPKShifts_Schedule",
                table: "Shifts_Schedule",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "XPKProducts_Essentals",
                table: "Products_Essentals",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "XPKOrders_Products",
                table: "Orders_Products",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "XPKEssentials_Essential_Orders",
                table: "Essentials_Essential_Orders",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_Shifts_Schedule_shift_id",
                table: "Shifts_Schedule",
                column: "shift_id");

            migrationBuilder.CreateIndex(
                name: "IX_Products_Essentals_essential_id",
                table: "Products_Essentals",
                column: "essential_id");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_Products_order_id",
                table: "Orders_Products",
                column: "order_id");

            migrationBuilder.CreateIndex(
                name: "IX_Essentials_Essential_Orders_essential_id",
                table: "Essentials_Essential_Orders",
                column: "essential_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "XPKShifts_Schedule",
                table: "Shifts_Schedule");

            migrationBuilder.DropIndex(
                name: "IX_Shifts_Schedule_shift_id",
                table: "Shifts_Schedule");

            migrationBuilder.DropPrimaryKey(
                name: "XPKProducts_Essentals",
                table: "Products_Essentals");

            migrationBuilder.DropIndex(
                name: "IX_Products_Essentals_essential_id",
                table: "Products_Essentals");

            migrationBuilder.DropPrimaryKey(
                name: "XPKOrders_Products",
                table: "Orders_Products");

            migrationBuilder.DropIndex(
                name: "IX_Orders_Products_order_id",
                table: "Orders_Products");

            migrationBuilder.DropPrimaryKey(
                name: "XPKEssentials_Essential_Orders",
                table: "Essentials_Essential_Orders");

            migrationBuilder.DropIndex(
                name: "IX_Essentials_Essential_Orders_essential_id",
                table: "Essentials_Essential_Orders");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "Shifts_Schedule");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "Products_Essentals");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "Orders_Products");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "Essentials_Essential_Orders");

            migrationBuilder.AddPrimaryKey(
                name: "XPKShifts_Schedule",
                table: "Shifts_Schedule",
                columns: new[] { "shift_id", "date" });

            migrationBuilder.AddPrimaryKey(
                name: "XPKProducts_Essentals",
                table: "Products_Essentals",
                columns: new[] { "essential_id", "product_id" });

            migrationBuilder.AddPrimaryKey(
                name: "XPKOrders_Products",
                table: "Orders_Products",
                columns: new[] { "order_id", "product_id" });

            migrationBuilder.AddPrimaryKey(
                name: "XPKEssentials_Essential_Orders",
                table: "Essentials_Essential_Orders",
                columns: new[] { "essential_id", "order_id" });
        }
    }
}
