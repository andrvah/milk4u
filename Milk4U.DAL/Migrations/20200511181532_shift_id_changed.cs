using Microsoft.EntityFrameworkCore.Migrations;

namespace Milk4U.DAL.Migrations
{
    public partial class shift_id_changed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "R_24",
                table: "Products");

            //migrationBuilder.DropIndex(
            //    name: "IX_Products_shift_id",
            //    table: "Products");

            migrationBuilder.DropColumn(
                name: "shift_id",
                table: "Products");

            migrationBuilder.AddColumn<int>(
                name: "ShiftId",
                table: "Warehouse",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ShiftsId",
                table: "Products",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Products_ShiftsId",
                table: "Products",
                column: "ShiftsId");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Shifts_ShiftsId",
                table: "Products",
                column: "ShiftsId",
                principalTable: "Shifts",
                principalColumn: "shift_id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_Shifts_ShiftsId",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Products_ShiftsId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "ShiftId",
                table: "Warehouse");

            migrationBuilder.DropColumn(
                name: "ShiftsId",
                table: "Products");

            migrationBuilder.AddColumn<int>(
                name: "shift_id",
                table: "Products",
                type: "int",
                nullable: true);

            //migrationBuilder.CreateIndex(
            //    name: "IX_Products_shift_id",
            //    table: "Products",
            //    column: "shift_id");

            migrationBuilder.AddForeignKey(
                name: "R_24",
                table: "Products",
                column: "shift_id",
                principalTable: "Shifts",
                principalColumn: "shift_id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
